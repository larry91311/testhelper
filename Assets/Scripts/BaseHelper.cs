using System.Collections.Generic;
using UnityEngine;

namespace UIHelper
{
    public enum LogicSelectionType
    {
        LogicOne,
        LogicTwo,
        LogicThree,
    }

    /// <summary>
        /// 貨幣類型
        /// </summary>
        public enum CurrencyCategories : int
        {
            None = 0,

            GoldCoin = 1,
            Diamond = 2,
            FriendCoin = 4,
            GuildCoin = 5,
            SoulCoin = 6,

            PlayerEXP = 101,
            VIPEXP = 102,
            HeroEXP = 103,
            EquipEXP = 104,
            GuildEXP = 105,
            Essence = 106,
            Powder = 107,

            ChallengeTicket_Elementary = 201,
            ChallengeTicket_Intermediate = 202,
            ChallengeTicket_Advanced = 203,

            LotteryTicket_Friend = 301,
            LotteryTicket_Bace = 302,
            LotteryTicket_Race = 303,
        }

    public class BaseHelper : MonoBehaviour
    {
        /// <summary>
        /// 物件
        /// </summary>
        public GameObject GameObj;
        
        //ANCHOR 結構
        public virtual void Awake ()
        {

        }

        public virtual void OnEnable ()
        {

        }

        public virtual void Start ()
        {

        }

        public virtual void Update ()
        {

        }

        public virtual void OnDisable ()
        {

        }

        public virtual void Dispose ()
        {

        }

        public virtual void Refresh ()
        {

        }

        public virtual void Initialize()
        {

        }

        public virtual void Inject(ControlModel helperModel)
        {

        }

        //ANCHOR 生成功能物件資料
        /// <summary>
        /// Button物件GameObject群
        /// </summary>
        /// <typeparam name="GameObject"></typeparam>
        /// <returns></returns>
        protected List<GameObject> CellGameObjectList = new List<GameObject>();     //TABCELL資料LIST

        /// <summary>
        /// Button物件Class群
        /// </summary>
        /// <typeparam name="BaseCell"></typeparam>
        /// <returns></returns>
        protected List<BaseCell> CellClassList = new List<BaseCell>();    //tab cell List

        /// <summary>
        /// Button物件Prefab
        /// </summary>
        [SerializeField]
        protected GameObject cellPrefab;
        
        //ANCHOR 生成功能版型資料
        /// <summary>
        /// 寬度
        /// </summary>
        protected float Width = 200.0f;

        /// <summary>
        /// 高度
        /// </summary>
        protected float Height = 200.0f;

        /// <summary>
        /// ButtonCell數量
        /// </summary>
        protected int ButtonAmount;         

        /// <summary>
        /// ButtonCell之間間距
        /// </summary>
        protected float ButtonGap;   

        /// <summary>
        /// ButtonCell版型方向
        /// </summary>     
        protected bool LayoutDirection = true;                    //true:橫、false:直

        /// <summary>
        /// ButtonCell生成方向
        /// </summary>
        protected bool ButtonDirection = false;            //true:從00向正向生，false:從00向負向生

        //ANCHOR 生成LocalField
        /// <summary>
        /// ButtonCell版型方向乘數
        /// </summary>
        protected int layoutDirectionValue = 0;                 //方向乘數

        /// <summary>
        /// ButtonCell生成方向
        /// </summary>
        protected int buttonDirectionValue = 0;
    }

    public class HelperModel  { }
}
