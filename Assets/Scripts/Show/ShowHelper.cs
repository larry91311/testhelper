//using System.Runtime.Intrinsics.Arm.Arm64;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AddressableAssets;




namespace UIHelper
{
    public enum ShowCellType
    {
        ShowCell,    //原生
        ResourceCell    //MiddleTemplate上面顯示的那種
    }

    public enum ShowHelperLogicType
    {
        InstantiateButtonCellGameObjectInRow,
        InstantiateButtonCellGameObjectInTransformArray,
    }

    public class ShowHelper : BaseHelper
    {
        //ANCHOR 邏輯資料
        /// <summary>
        /// ShowCell類型
        /// </summary>
        private ShowCellType ShowCellType;       

        /// <summary>
        /// Show顯示邏輯類型
        /// </summary>
        private ShowHelperLogicType ShowLogicType = ShowHelperLogicType.InstantiateButtonCellGameObjectInRow;   //預設一

        //ANCHOR 功能資料
        /// <summary>
        /// 換圖原始圖集合
        /// </summary>
        /// <typeparam name="string"></typeparam>
        /// <returns></returns>
        protected List<string> originSpriteNameList = new List<string>();
        protected List<Sprite> originList = new List<Sprite>();

        /// <summary>
        /// 取位置按照Transform[]位置邏輯 的 Transform[]位置
        /// </summary>
        protected Transform[] instantiateTransforms = new Transform[0];
        
        #region Tab:ControlObjectCycle
        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update()
        {

        }

        public override void Dispose()
        {

        }
        #endregion
        #region Tab:InjectLoop
        //ANCHOR 注入資料
        public override void Inject(ControlModel helperModel)
        {
            var showHelperModel = helperModel as ShowHelperModel;

            //ANCHOR 注入ShowCell類型
            ShowCellType = showHelperModel.CellType; 

            //ANCHOR 注入ShowHelper邏輯類型
            ShowLogicType = showHelperModel.LogicType;              

            //ANCHOR 注入ShowHelper數據
            ButtonAmount = showHelperModel.CellAmount;           
            Width = showHelperModel.Width;                          
            Height = showHelperModel.Height;                         
            ButtonGap = showHelperModel.ButtonGap;                 
            LayoutDirection = showHelperModel.LayoutDirection;     
            ButtonDirection = showHelperModel.ButtonDirection;     

            //ANCHOR 注入ShowHelper邏輯類型需要資料
            switch (ShowLogicType)
            {
                case ShowHelperLogicType.InstantiateButtonCellGameObjectInRow:
                    {

                    }
                    break;

                case ShowHelperLogicType.InstantiateButtonCellGameObjectInTransformArray:
                    {
                        instantiateTransforms = showHelperModel.InstantiateTransforms;
                    }
                    break;

                default:
                    {
                        UnityEngine.Debug.LogError("沒有這個邏輯");
                    }
                    break;
            }
            
            CreateTab();
        }
        #endregion

        #region Tab:CREATEDESTROYTAB
        void CreateTab()
        {
            switch (ShowLogicType)
            {
                case ShowHelperLogicType.InstantiateButtonCellGameObjectInRow:
                    {
                        PresetGameObjectBeforeLogic_InstantiateButtonCellGameObjectInRow();

                        PresetValueBeforeLogic_InstantiateButtonCellGameObjectInRow();  //設定橫向或直向

                        InstantiateButtonLogic_InstantiateButtonCellGameObjectInRow();  //產生Cell的物件和賦值

                        BindEventLogic_InstantiateButtonCellGameObjectInRow();

                        // LoadSpriteList
                        // (
                        //     originSpriteNameList,
                        //     originList
                        // );
                    }
                    break;

                case ShowHelperLogicType.InstantiateButtonCellGameObjectInTransformArray:
                    {
                        PresetGameObjectBeforeLogic_InstantiateButtonCellGameObjectInTransformArray();

                        PresetValueBeforeLogic_InstantiateButtonCellGameObjectInTransformArray();

                        InstantiateButtonLogic_InstantiateButtonCellGameObjectInTransformArray();

                        BindEventLogic_InstantiateButtonCellGameObjectInTransformArray();

                        LoadSpriteList
                        (
                            originSpriteNameList,
                            originList
                        );
                    }
                    break;
            }
        }
        #endregion

        

        #region PresetGameObjectBeforeLogic_Template
        #endregion
        #region PresetValueBeforeLogic_Template
        #endregion
        #region InstantiateButtonLogic_Template
        #endregion
        #region BindEventLogic_Template
        #endregion
        #region ChangeState_Template
        #endregion

        #region PresetGameObjectBeforeLogic_General
        #endregion
        #region PresetValueBeforeLogic_General
        protected void SetLayoutDirectionIntAndButtonInstanDirectionInt_General()
        {
            layoutDirectionValue = LayoutDirection ? 1 : -1;
            buttonDirectionValue = ButtonDirection ? 1 : -1;
        }
        #endregion
        #region InstantiateButtonLogic_General
        void CreateShowCell_General()
        {
            for (int _index = 0; _index < ButtonAmount; _index++)
            {
                ControlModel model = null;

                switch (ShowCellType)
                {
                    case ShowCellType.ShowCell:
                        {
                            model = new ShowCellModel()
                            {
                                Index = _index,
                                //IconSpriteID = showRowList[_index].iconSpriteID,
                            };
                        }
                        break;

                    case ShowCellType.ResourceCell:
                        {
                            // if(resourceCategoryList==null)
                            // {
                            //     return;
                            // }
                            // model = new ResourceCellModel()
                            // {
                            //     // ID = (int)resourceCategoryList[_index],
                            //     // ResourceQuantity = ItemProcessor.GetCurrencyQuantity(resourceCategoryList[_index])
                            // };
                        }
                        break;
                }
                BaseShowButtonCell showCell = new BaseShowButtonCell();
                showCell.Initialize(CellGameObjectList[_index]);
                showCell.Inject(model);
                CellClassList.Add(showCell);
            }
        }
        #endregion
        #region BindEventLogic_General
        #endregion
        #region ChangeState_General
        #endregion

        #region PresetGameObjectBeforeLogic_LogicOne
        #endregion
        #region PresetValueBeforeLogic_LogicOne
        #endregion
        #region InstantiateButtonLogic_LogicOne
        protected void InstantiateButtonCellGameObjectInRow()
        {
            for (int _index = 0; _index < ButtonAmount; _index++)
            {
                //生成
                GameObject instanButton = GameObject.Instantiate(cellPrefab, GameObj.transform);

                //開
                instanButton.gameObject.SetActive(true);

                //設名
                instanButton.name = _index.ToString();

                #region RectTransform設定
                RectTransform instanRectTran = instanButton.transform as RectTransform;
                //大小
                instanRectTran.sizeDelta = Vector2.right * Width + Vector2.up * Height;
                //生成方向，正值:左到右，負值:右到左
                if (LayoutDirection == true)
                {
                    var x = (ButtonGap) * buttonDirectionValue * _index * Vector2.right;
                    instanRectTran.anchoredPosition = x;   //橫
                }
                else
                {
                    var y = (ButtonGap) * buttonDirectionValue * _index * Vector2.up;
                    instanRectTran.anchoredPosition = y;   //直
                }
                #endregion

                #region 添加進TabCellList
                //添加進TABCELL LIST
                CellGameObjectList.Add(instanButton);
                #endregion
            }
        }
        #endregion
        #region LoadSprites
        protected void LoadSpriteList(List<string> spriteNames, List<Sprite> spriteList, UnityAction delayAction = null)
        {
            var loadCount = 0;
            var listCount = spriteNames.Count;
            for (int i = 0; i < listCount; i++)
            {
                spriteList.Add(null);
            }
            for (int _index = 0; _index < listCount; _index++)
            {
                var temp = _index;
                Addressables.LoadAssetAsync<Sprite>(spriteNames[_index]).Completed += asyncOperationHandle =>
                {
                    spriteList[temp] = asyncOperationHandle.Result;

                    loadCount++;

                    if (loadCount == listCount)
                    {
                        if (delayAction != null)
                        {
                            delayAction();
                        }
                    }
                };
            }
        }
        #endregion
        #region BindEventLogic_LogicOne
        void BindCurrentIndexChangeEvent_LogicOne()
        {

        }
        #endregion
        #region ChangeState_LogicOne
        #endregion

        #region PresetGameObjectBeforeLogic_LogicTwo
        #endregion
        #region PresetValueBeforeLogic_LogicTwo
        #endregion
        #region InstantiateButtonLogic_LogicTwo
        protected void InstantiateButtonCellGameObjectInTransformArray()
        {
            for (int _index = 0; _index < ButtonAmount; _index++)
            {
                //生成
                GameObject instanButton = GameObject.Instantiate(cellPrefab, instantiateTransforms[_index]);

                //開
                instanButton.gameObject.SetActive(true);

                //設名
                instanButton.name = _index.ToString();

                #region RectTransform設定
                RectTransform instanRectTran = instanButton.transform as RectTransform;
                instanRectTran.anchoredPosition = Vector2.zero;
                // //生成方向，正值:左到右，負值:右到左
                // if (LayoutDirection == true)
                // {
                //     var x = (ButtonGap) * buttonDirectionValue * _index * Vector2.right;
                //     instanRectTran.anchoredPosition = x;   //橫
                // }
                // else
                // {
                //     var y = (ButtonGap) * buttonDirectionValue * _index * Vector2.up;
                //     instanRectTran.anchoredPosition = y;   //直
                // }
                #endregion

                #region 添加進TabCellList
                //添加進TABCELL LIST
                CellGameObjectList.Add(instanButton);
                #endregion
            }
        }
        #endregion
        #region BindEventLogic_LogicTwo
        void BindCurrentIndexChangeEvent_LogicTwo()
        {

        }
        #endregion
        #region ChangeState_LogicTwo
        #endregion

        #region 架構
        #region Template
        void PresetGameObjectBeforeLogic_Template() { }
        void PresetValueBeforeLogic_Template() { }
        void InstantiateButtonLogic_Template() { }
        void BindEventLogic_Template() { }
        void ChangeState_Template() { }
        #endregion

        #region 邏輯一:還未有TabButton，新生成
        void PresetGameObjectBeforeLogic_InstantiateButtonCellGameObjectInRow() 
        { 

        }

        void PresetValueBeforeLogic_InstantiateButtonCellGameObjectInRow()
        {
            SetLayoutDirectionIntAndButtonInstanDirectionInt_General();
        }

        void InstantiateButtonLogic_InstantiateButtonCellGameObjectInRow()
        {
            InstantiateButtonCellGameObjectInRow();
            CreateShowCell_General();
        }

        void BindEventLogic_InstantiateButtonCellGameObjectInRow()
        {
            BindCurrentIndexChangeEvent_LogicOne();
        }

        void ChangeState_LogicOne()
        {

        }
        #endregion

        #region 邏輯二:生在Transform Array下
        void PresetGameObjectBeforeLogic_InstantiateButtonCellGameObjectInTransformArray() 
        {

        }

        void PresetValueBeforeLogic_InstantiateButtonCellGameObjectInTransformArray()
        {
            SetLayoutDirectionIntAndButtonInstanDirectionInt_General();
        }

        void InstantiateButtonLogic_InstantiateButtonCellGameObjectInTransformArray()
        {
            InstantiateButtonCellGameObjectInTransformArray();
            CreateShowCell_General();
        }

        void BindEventLogic_InstantiateButtonCellGameObjectInTransformArray()
        {
            BindCurrentIndexChangeEvent_LogicOne();
        }

        void ChangeState_LogicTwo()
        {

        }
        #endregion
        #endregion
    }

    public class ShowHelperModel : ControlModel
    {
        //Data 
        public ShowCellType CellType { get; set; }
        public int CellAmount { get; set; }    //總數
        public float Width { get; set; } = 200;       //寬
        public float Height { get; set; } = 200;      //高
        public float ButtonGap { get; set; }   //間距
        public bool LayoutDirection { get; set; }   //板向
        public bool ButtonDirection { get; set; }   //Cell向

        public List<string> OriginSpriteNameList { get; set; } = new List<string>();
        public List<CurrencyCategories> ResourceCategoryList { get; set; } = new List<CurrencyCategories>();

        //LogicTwo
        public Transform[] InstantiateTransforms { get; set; } = new Transform[0];

        //LogicType
        public ShowHelperLogicType LogicType { get; set; }
    }
}
