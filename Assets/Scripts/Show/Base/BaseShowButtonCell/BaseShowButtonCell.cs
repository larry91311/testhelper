using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.AddressableAssets;

namespace UIHelper
{
    public class ShowCellModel:ControlModel
    {
        public int Index;
        public string Name;
        public string IconSpriteID;
        public string IconText;
    }
    public class BaseShowButtonCell : BaseCell
    {
        /// <summary>
        /// 圖片
        /// </summary>
        [SerializeField]
        protected Image image;

        /// <summary>
        /// 文字
        /// </summary>
        [SerializeField]
        protected TextMeshProUGUI text;

        public override void Initialize (GameObject gameObject)
        {
            base.Initialize ( gameObject );
        }

        public override void Inject(ControlModel model)
        {
            base.Inject ( model );

            var showCellModel=model as ShowCellModel;

            //加載圖片
            LoadButtonSpriteByID(showCellModel.IconSpriteID);   
            //加載數據文字
            text.text=showCellModel.IconText;               
        }

        public void RefreshIconText(string text)
        {
            this.text.text= text;
        }

        protected void LoadButtonSpriteByID ( string spriteAddress )
        {
            Addressables.LoadAssetAsync<Sprite>(spriteAddress).Completed += asyncOperationHandle =>   //載圖
            {
                if (image != null)   //異步保險，避免開關視窗後 取用已銷毀的
                {
                    image.sprite = asyncOperationHandle.Result;
                }
            };
        }

        
    }
}

