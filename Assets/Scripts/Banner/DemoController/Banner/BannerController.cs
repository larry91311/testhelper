using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace UIHelper
{
    public class BannerController : MonoBehaviour
    {   
        [SerializeField]
        private BannerHelper bannerHelper;      

        public void Start ()
        {
            CreateBannerLoop();
        }

        public void OnBtnCloseClick()
        {
            UIManager.Instance.ClosePanel(this.name);
        }

        void CreateBannerLoop()
        {
            BannerHelperModel helperModel=new BannerHelperModel()   //Model
            {
                //Data
                ItemNum = 3,
                ItemXGap = -10,
                DataNum = 5,

                //Feature
                LoopActive=false,

                //Action
                OnBannerButtonClickAction=OnBannerClick,
            };
            helperModel.SizeList=new List<float>(){ 0.8f,0.9f,1.1f,0.9f,0.8f };

            helperModel.SpriteIDList=new List<string>(){ "RedCurtain","OrangeCurtain","YellowCurtain","GreenCurtain","BlueCurtain" };

            bannerHelper.Initialize();  //InitialReference

            bannerHelper.Inject(helperModel);    //InjectModel
        }


        void OnBannerClick(int index)
        {
            switch(index)
            {
                case 0:
                {
                    Debug.LogError("BannerMiddle:"+index.ToString());
                }
                break;

                case 1:
                {
                    Debug.LogError("BannerMiddle:"+index.ToString());
                }
                break;
                case 2:
                {
                    Debug.LogError("BannerMiddle:"+index.ToString());
                }
                break;
                case 3:
                {
                    Debug.LogError("BannerMiddle:"+index.ToString());
                }
                break;
                case 4:
                {
                    Debug.LogError("BannerMiddle:"+index.ToString());
                }
                break;
            }
        }

        
    }
}

