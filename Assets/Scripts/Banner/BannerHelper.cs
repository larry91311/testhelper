using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.Events;

namespace UIHelper
{
    using UnityEngine.AddressableAssets;

    public class BannerHelperModel :ControlModel
    {
        //Data
        public int ItemNum { get; set; }
        public int DataNum { get; set; }
        public float ItemXGap { get; set; }
        public List<string> SpriteIDList { get; set;}=new List<string>();

        //Feature
        public bool LoopActive { get; set; }

        //Reference
        public GameObject RevealCell { get; set; }
        public Image BlockImage { get; set; }
        public Mask MaskBuffer { get; set; }
        public RectTransform ContentRectTransform { get; set; }
        public RectTransform ScrollRectRectTransform { get; set; }


        //Prefab information
        public GameObject CellPrefab { get; set; }

        //EachSetSize 
        public List<float> SizeList { get; set; }

        //Action 
        public UnityAction<int> OnBannerButtonClickAction;
    }

    public class BannerHelper : BaseHelper
    {
        //ANCHOR 設定
        /// <summary>
        /// 活動頁物件總數
        /// </summary>
        private int ItemNum=1;  

        /// <summary>
        /// 活動頁資料數量
        /// </summary>
        private int DataNum=0; 

        /// <summary>
        /// 活動頁間距
        /// </summary>
        private float ItemGap=-10.0f;  

        /// <summary>
        /// 無限循環開關
        /// </summary>
        private bool RestrictLoopingActive = true;   

        /// <summary>
        /// 活動頁圖片資源名稱List
        /// </summary>
        /// <typeparam name="string"></typeparam>
        /// <returns></returns>
        private List<string> SpriteIDList=new List<string>();

        //ANCHOR 物件
        /// <summary>
        /// 活動頁物件List
        /// </summary>
        /// <typeparam name="GameObject"></typeparam>
        /// <returns></returns>
        private List<GameObject> ItemCellList=new List<GameObject>();   

        /// <summary>
        /// 活動頁Class List
        /// </summary>
        /// <typeparam name="BannerCell"></typeparam>
        /// <returns></returns>
        private List<BannerCell> bannerCellList=new List<BannerCell>();
        
        [Header("顯示屏")]
        /// <summary>
        /// 顯示屏物件
        /// </summary>
        private GameObject RevealCell;    

        /// <summary>
        /// 顯示屏圖片
        /// </summary>
        [SerializeField]
        private Image revealImage;

        [Header("遮擋最中間活動頁UI行為的圖片")]
        /// <summary>
        /// 遮擋最中間活動頁UI行為的圖片
        /// </summary>
        [SerializeField]
        private Image blockImage;    
        
        [Header("ScrollView物件")]
        /// <summary>
        /// Mask，目前不去使用
        /// </summary>
        [SerializeField]
        private Mask maskBuffer;    

        /// <summary>
        /// ScrollView Content的RectTransform
        /// </summary>
        [SerializeField]
        private RectTransform contentRectTran;   

        /// <summary>
        /// ScrollView ScrollRect的RectTransform
        /// </summary>
        [SerializeField]
        private RectTransform scrollRectRectTran;  

        //ANCHOR LocalField
        /// <summary>
        /// 及時Index
        /// </summary>
        private int instantFirstIndex = 0;
        private int instantFirstModelIndex = 0;
        private int instantMiddleIndex=0;

        /// <summary>
        /// 其他Index
        /// </summary>
        private int middleIndex = 0;
        private int lastIndex = 0;   
        private int firstIndex = 0;
        private int lastModelIndex = 0;
        private int firstModelIndex = 0;
        private float xPositivePosBuffer = 0;
        private float xNegativePosBuffer = 0;

        private float itemWidth=100.0f;          
        private float itemWidthBuffer;

        private float itemHeight=100.0f;
        private float itemHeightBuffer;   //物高

        //BoarderGap
        private float boarderXGap=0;
        private float boarderXGapBuffer;   //邊X 背景圖相對SCROLLVIEW 的 X間距

        private float boarderYGap=0;
        private float boarderYGapBuffer;  //邊Y 背景圖相對SCROLLVIEW 的 Y間距

        //ContainRow
        private float containRow=0.4f;
        private float containRowBuffer;

        //Drag
        private float moveTime=0.7f;
        private float positionSumBuffer=0;

        //Mask
        private bool maskActive=false;       
        private float compareBackgroundX=0.7f;
        private float compareBackgroundY=1.8f;
        #region BannerD:EachSetSize
        private bool eachSetSize=true;
        private float setSizeTime=0.5f;
        private List<float> sizeList=new List<float>(){};
        private float sizePositionBuffer;      //儲存遞增遞加的不同SIZE的position      
        private List<float> sizePositionList=new List<float>();      
        #endregion
        #region BannerD:Inject
        
        private bool triggerEventActive=false;
        private UnityEvent<int> onBannerButtonClickEvent=new UnityEvent<int>();
        private UnityAction<int> OnBannerButtonClickAction;
        #endregion

        public List<int> IndexList=new List<int>(){ 0,1,2,3,4,5,6,7,8 };

        #region Banner:ControlObjectCycle
        public override void Initialize ()
        {
            base.Initialize ();
        }
        
        public override void Update()
        {
            base.Update();
            OnScrollDetectReachBoundary();
        }
        #endregion
        #region Banner:Inject
        public override void Inject(ControlModel helperModel)
        {
            var bannerModel=helperModel as BannerHelperModel;
            //Data
            ItemNum=bannerModel.ItemNum;
            ItemGap=bannerModel.ItemXGap;
            DataNum=bannerModel.DataNum;
            SpriteIDList.AddRange(bannerModel.SpriteIDList);
            
            //SizeList
            sizeList.AddRange(bannerModel.SizeList);

            //Feature
            RestrictLoopingActive=bannerModel.LoopActive;

            //Action
            OnBannerButtonClickAction=bannerModel.OnBannerButtonClickAction;
            
            DestroyBanner();
            CreateBanner();
        }
        #endregion

        protected void AddEvent(GameObject obj, EventTriggerType type, UnityAction<BaseEventData> action)
        {
            EventTrigger trigger = obj.GetComponent<EventTrigger>();
            var eventTrigger = new EventTrigger.Entry();
            eventTrigger.eventID = type;
            eventTrigger.callback.AddListener(action);
            trigger.triggers.Add(eventTrigger);
        }

        #region 共用Function
        void PushBackValue(ref int target,int targetReferenceValue,int minValue,int maxValue,int pushBackValue)
        {
            if (targetReferenceValue < minValue)
            {
                while (target < minValue)
                {
                    target += pushBackValue;
                }
            }
            if (targetReferenceValue > maxValue)
            {
                while (target > maxValue)
                {
                    target -= pushBackValue;
                }
            }
        }
        #endregion
        #region Banner:CreateDestroyHorizon
        public void CreateBanner()
        {
            InitialPositionData(sizePositionList);    //初始大小不同但等距的位置LIST
            
            InstanHoriaonItemsForLoop();     //生成

            CreateBannerCell();

            SetLayouts();   //設定LAYOUT
        }

        public void DestroyBanner()
        {
            int listCount=ItemCellList.Count;
            for(int i=0;i<listCount;i++)
            {
                GameObject.Destroy(ItemCellList[i]);
            }
            ItemCellList.Clear();
        }
        #endregion
        #region Banner:InitialData
        public void InitialPositionData(List<float> sizePositionList)
        {
            for(int _index=0;_index<ItemNum;_index++)
            {
                int clampMinusOne=Mathf.Clamp((_index-1),0,100);     //>=0
                int clampOrigin=Mathf.Clamp((_index),0,100);     //>=0
                if(_index!=0)
                {
                    sizePositionBuffer+= (sizeList[clampMinusOne]*itemWidth/2+ItemGap+sizeList[clampOrigin]*itemWidth/2);
                }
                sizePositionList.Add(sizePositionBuffer);
            }
        }
        #endregion
        #region Banner:InstantiateItems
        public void InstanHoriaonItemsForLoop()
        {
            if(OnBannerButtonClickAction!=null)
            {
                onBannerButtonClickEvent.AddListener(OnBannerButtonClickAction);   //添
            }
            
            #region 新-生成流程
            for (int itemNumIndex = 0; itemNumIndex < (Mathf.Ceil((float)ItemNum / 1)); itemNumIndex++)
            {
                for (int zero = 0; zero < 1; zero++)
                {
                    //生成
                    GameObject newItem = MonoBehaviour.Instantiate(cellPrefab,contentRectTran.transform);

                    //開 
                    newItem.gameObject.SetActive(true);
                    
                    //設父
                    newItem.transform.SetParent(contentRectTran);

                    //設名
                    newItem.name = itemNumIndex.ToString();

                    //設RectTransform
                    //设定锚点以及锚点位置
                    RectTransform _rect = (RectTransform)(newItem.transform);

                    _rect.pivot = new Vector2(0.5f, 0.5f);       //錨點
                    _rect.anchorMin = new Vector2(0.5f, 0.5f);
                    _rect.anchorMax = new Vector2(0.5f, 0.5f);
                    
                    
                    _rect.anchoredPosition = Vector2.right*((((float)itemNumIndex-(float)ItemNum/2) * (ItemGap)));  //位置
                    

                    //BUTTON上移動一格邏輯
                    var button=newItem.transform.GetChild(0).gameObject;
                    AddEvent(
                        button.gameObject,
                        EventTriggerType.PointerClick,
                        (data)=>
                        {
                            ListenerClick((PointerEventData)data);
                        }
                    );
                    AddEvent(
                        button.gameObject,
                        EventTriggerType.EndDrag,
                        (data)=>
                        {
                            ListenerOnEndDrag((PointerEventData)data);
                        }
                    );
        
                    //将游戏对象按顺序加到显示list当中
                    ItemCellList.Add(newItem);
                }
            }
            //背景上移動一格邏輯
            var scrollRectGameObject=scrollRectRectTran.gameObject;        
            AddEvent(
                        scrollRectGameObject.gameObject,
                        EventTriggerType.PointerClick,
                        (data)=>
                        {
                            ListenerClick((PointerEventData)data);
                        }
                    );
            AddEvent(
                        scrollRectGameObject.gameObject,
                        EventTriggerType.EndDrag,
                        (data)=>
                        {
                            ListenerOnEndDrag((PointerEventData)data);
                        }
                    );

            //初始CONTENT XPOS BUFFER位置
            positionSumBuffer=contentRectTran.anchoredPosition.x;   

            //設定RollIndex
            lastIndex = ItemCellList.Count - 1;
            //設定ModelIndex
            lastModelIndex=ItemCellList.Count - 1;  
            //設定InstantIndex
            instantFirstIndex=0;                  
            #endregion
        }

        void CreateBannerCell()
        {
            for(int _index=0;_index<ItemNum;_index++)
            {
                var bannerCell=ItemCellList[_index].GetComponent<BannerCell>();

                bannerCell.Initialize (ItemCellList[_index]);
                
                bannerCell.Inject(
                    new BannerCellModel()
                    {
                        Index=_index,
                        SpriteID=SpriteIDList[_index]
                    }
                );
                
                bannerCellList.Add(bannerCell);
            }
        }
        #endregion
        #region Banner:UIEventLogic
        void ListenerClick(PointerEventData eventData)
        {
            float direction=(eventData.position.x>Screen.width/2)?1:-1;      //是否>中

            if(RestrictLoopingActive==true&&direction==1&&firstIndex==-(DataNum-1))
            {
                return;
            }
            if(RestrictLoopingActive==true&&direction==-1&&firstIndex==0)
            {
                return;
            }

            positionSumBuffer+=direction*(itemWidth+ItemGap);   //累積位置
            
            if(RestrictLoopingActive==true)  //計算後校正
            {
                positionSumBuffer=Mathf.Clamp(positionSumBuffer,0,((DataNum-1)*(itemWidth+ItemGap)));
            }

            contentRectTran.DOPause();
            contentRectTran.DOAnchorPosX(positionSumBuffer,moveTime).onComplete=()=>{
                triggerEventActive=true;
            };  //核心動畫

            //instant part
            instantFirstIndex-=(int)direction;
            PushBackValue(ref instantFirstIndex,instantFirstIndex,0,ItemNum - 1,ItemNum);
            instantFirstModelIndex-=(int)direction;
            PushBackValue(ref instantFirstModelIndex,instantFirstModelIndex,0,DataNum - 1,DataNum);
            int middle=instantFirstModelIndex+ItemNum/2;    //計算中間
            PushBackValue(ref middle,middle,0,DataNum-1,DataNum);
            Debug.LogError("InstantMiddle:"+middle);
        }

        void ListenerOnEndDrag(PointerEventData eventData)
        {
            triggerEventActive=true;

            float direction=(eventData.delta.x>0)? 1 : -1 ;        //位差方向

            if(RestrictLoopingActive==true&&direction==1&&firstIndex==-(DataNum-1))
            {
                return;
            }
            if(RestrictLoopingActive==true&&direction==-1&&firstIndex==0)
            {
                return;
            }

            positionSumBuffer+=direction*(itemWidth+ItemGap);         //計算位置

            if(RestrictLoopingActive==true)   //計算後校正
            {
                positionSumBuffer=Mathf.Clamp(positionSumBuffer,0,((DataNum-1)*(itemWidth+ItemGap)));
            }
            contentRectTran.DOPause();
            contentRectTran.DOAnchorPosX(positionSumBuffer,moveTime).onComplete=()=>{
                triggerEventActive=true;
            };   //核心動畫

            //instant part
            instantFirstIndex-=(int)direction;
            PushBackValue(ref instantFirstIndex,instantFirstIndex,0,ItemNum - 1,ItemNum);
            instantFirstModelIndex-=(int)direction;
            PushBackValue(ref instantFirstModelIndex,instantFirstModelIndex,0,DataNum - 1,DataNum);
            int middle=instantFirstModelIndex+ItemNum/2;    //計算中間
            PushBackValue(ref middle,middle,0,DataNum-1,DataNum);
            Debug.LogError("InstantMiddle:"+middle);
        }
        #endregion
        #region Banner:SetLayouts
        void SetLayouts()
        {
            SetContentLayout();
            SetScrollRectLayout();
            SetBackgroundLayout();
            SetBackgroundLayoutAtBottom();
            SetMask();
            //SetScale(maskBuffer.transform as RectTransform,4.5f,3,2);
        }

        void SetContentLayout()  //設定SIZE 錨點MIN MAX
        {
            float sizeX1 = 0;
            float sizeY1 = (Mathf.Ceil((float)ItemNum / 1) * itemWidth);
                
        
            contentRectTran.sizeDelta = Vector2.right * sizeY1 + Vector2.up * sizeX1;
            
            contentRectTran.anchorMin = Vector2.right * 0 + Vector2.up * 0;    //左側直線
            contentRectTran.anchorMax = Vector2.right * 0 + Vector2.up * 1;
        }

        void SetScrollRectLayout() //設定SIZE
        {
            float sizeX = itemHeight * 1;
            float sizeY = ItemNum * itemWidth + ItemGap * (ItemNum - 1);

            scrollRectRectTran.sizeDelta = Vector2.right * sizeY + Vector2.up * sizeX;
        }

        void SetBackgroundLayout()
        {
            //backgroundRectTran.sizeDelta = Vector2.right*(scrollRectRectTran.sizeDelta.x + (boarderXGap * 2))+Vector2.up*(scrollRectRectTran.sizeDelta.y + (boarderYGap * 2));
        }

        void SetBackgroundLayoutAtBottom()
        {
            //backgroundRectTran.SetSiblingIndex(0);
        }

        void SetMask()
        {
            if(maskBuffer==null)
            {
                return;
            }

            if(maskActive==true)
            {
                maskBuffer.enabled=true;
                SetMaskWidthHeightMultiply(compareBackgroundX,compareBackgroundY);
            }
            else
            {
                maskBuffer.enabled=false;
            }
        }

        void SetMaskWidthHeightMultiply(float xMultiplyValue,float yMultiplyValue)
        {
            var maskRectTransform=maskBuffer.transform as RectTransform;
            maskRectTransform.sizeDelta = Vector2.right*(scrollRectRectTran.sizeDelta.x*xMultiplyValue)+Vector2.up*(scrollRectRectTran.sizeDelta.y*yMultiplyValue);
        }

        void SetScale(RectTransform rectTransfrom,float xMultiplyValue,float yMultiplyValue,float zMultiplyValue)
        {
            rectTransfrom.localScale=Vector3.right*xMultiplyValue+Vector3.up*yMultiplyValue+Vector3.forward*zMultiplyValue;
        }
        #endregion
        #region Banner:OnScroll
        public void OnScrollDetectReachBoundary()
        {
            float direction=(Input.mousePosition.x>Screen.width/2)?1:-1;      //是否>中

            if(RestrictLoopingActive==true&&direction==1&&firstIndex==-(DataNum-1))
            {
                return;
            }
            if(RestrictLoopingActive==true&&direction==-1&&firstIndex==0)
            {
                return;
            }

            while (-contentRectTran.anchoredPosition.x - (itemWidth) * (containRow - 1) > (itemWidth + ItemGap) * Mathf.Ceil(firstIndex / 1))
            {
                for (int zero = 0; zero < 1; zero++)
                {
                    #region 新-更新流程
                    //取得頭部
                    GameObject _firstItem = ItemCellList[0];  //取得頭部 每次拔除頭部=拔1 拔2 拔3  

                    //取得頭部RECTTRAN
                    RectTransform _firstRect = ((RectTransform)_firstItem.transform);           

                    //計算位置
                    if (zero == 0)
                    {
                        xPositivePosBuffer = Mathf.Ceil(((lastIndex + 1) / 1)-(int)ItemNum/2)  * (itemWidth + ItemGap);
                    }

                    //設定位置
                    _firstRect.anchoredPosition = Vector2.right * xPositivePosBuffer + Vector2.up * _firstRect.anchoredPosition.y;

                    //移除頭部
                    ItemCellList.RemoveAt(0);
                    //添加到尾部
                    ItemCellList.Add(_firstItem);

                    //完成動作INDEX增加
                    firstIndex++;
                    lastIndex++;

                    firstModelIndex++;
                    lastModelIndex++;

                    //修改显示
                    _firstItem.name = lastIndex.ToString();

                    //ROLL INDEX 過上限過下限回推
                    int lastNameNum=lastIndex;
                    PushBackValue(ref lastNameNum,lastIndex,0,ItemNum-1,ItemNum);

                    _firstItem.name = lastNameNum.ToString();

                    //firstModelIndex 過上限過下限回推 
                    PushBackValue(ref firstModelIndex,firstModelIndex,0,DataNum-1,DataNum);

                    //lastModelIndex 過上限過下限回推  
                    PushBackValue(ref lastModelIndex,lastModelIndex,0,DataNum-1,DataNum);

                    if (_firstItem.gameObject.activeInHierarchy == false)
                    {
                        _firstItem.gameObject.SetActive(true);
                    }

                    #endregion
                    EachSetSize();
                    SetItemByIndex(lastNameNum,lastModelIndex);
                }
            }
            //从右往左
            while (-contentRectTran.anchoredPosition.x - (itemWidth) * (containRow - 1) < (itemWidth + ItemGap) * Mathf.Ceil(firstIndex / 1))
            {
                for (int zero = 0; zero < 1; zero++)
                {
                    #region 新-更新流程
                    //取得尾部
                    GameObject _lastItem = ItemCellList[ItemCellList.Count - 1];

                    //取得尾部RECTTRAN
                    RectTransform _lastRect = ((RectTransform)_lastItem.transform);

                    //計算位置
                    if (zero == 0)
                    {
                        xNegativePosBuffer = Mathf.Ceil(((firstIndex - 1) / 1)-(int)ItemNum/2) * (itemWidth + ItemGap);
                    }

                    //賦予位置
                    _lastRect.anchoredPosition = Vector2.right * xNegativePosBuffer + Vector2.up * _lastRect.anchoredPosition.y;
                        
                    //移除尾部
                    ItemCellList.RemoveAt(ItemCellList.Count - 1);
                    //添加到頭部
                    ItemCellList.Insert(0, _lastItem);

                    //完成所有動作INDEX--
                    firstIndex--;
                    lastIndex--;

                    firstModelIndex--;
                    lastModelIndex--;

                    //修改显示
                    _lastItem.name = firstIndex.ToString();

                    //ROLL INDEX 過上限過下限回推
                    int lastNameNum=firstIndex;
                    PushBackValue(ref lastNameNum,firstIndex,0,ItemNum - 1,ItemNum);

                    _lastItem.name = lastNameNum.ToString();

                    //lastModelIndex  過上限過下限回推
                    PushBackValue(ref lastModelIndex,lastModelIndex,0,DataNum-1,DataNum);

                    //first MODEL INDEX 過上限過下限回推
                    PushBackValue(ref firstModelIndex,firstModelIndex,0,DataNum-1,DataNum);

                    
                    if (_lastItem.gameObject.activeInHierarchy == false)
                    {
                        _lastItem.gameObject.SetActive(true);
                    }
                    #endregion
                    EachSetSize();
                    SetItemByIndex(lastNameNum,firstModelIndex);
                }
            }

            int middle=firstModelIndex+ItemNum/2;    //計算中間
            PushBackValue(ref middle,middle,0,DataNum-1,DataNum);
            middleIndex=middle;
            
            SetRevealScreen(middle);
        }
        #endregion
        #region Banner:LoopEvent
        void SetRevealScreen(int index)
        {
            var BannerCellModel=new BannerCellModel()
            {
                Index=index,
                SpriteID=SpriteIDList[index]
            };
            LoadSpriteByID(BannerCellModel.SpriteID,revealImage);


            //顯示中間完觸發 傳到外面參數的事件
            if(triggerEventActive==false)
            {
                return;
            }
            onBannerButtonClickEvent?.Invoke(index);
            triggerEventActive=false;
        }

        void LoadSpriteByID ( string spriteAddress ,Image imageBuffer)
        {
            // Addressables.LoadAssetAsync<Sprite> ( spriteAddress ).Completed += asyncOperationHandle =>
            // {
            //     imageBuffer.sprite = asyncOperationHandle.Result;
            // };

            Sprite loadSprite= Utility.AssetRelate.ResourcesLoadCheckNull<Sprite>(spriteAddress);
            imageBuffer.sprite = loadSprite;
        }

        void SetItemByIndex(int rollIndex,int dataIndex)
        {
            // if(bannerCellList.Count==0)
            // {
            //     return;
            // }

            //Debug.LogError("bannerCellList.Count:"+bannerCellList.Count+"/"+"SpriteIDList"+SpriteIDList.Count+"/"+"dataIndex:"+dataIndex+"/"+"rollIndex:"+rollIndex);
            bannerCellList[rollIndex].Inject
            (
                new BannerCellModel()
                {
                    Index=dataIndex,
                    SpriteID=SpriteIDList[dataIndex]
                }
            );
        }
        #endregion
        #region Banner:EachSetSizeFunctions
        public void EachSetSize()
        {
            if(eachSetSize==true)
            {
                #region 新-EachSetSize流程
                if(sizeList.Count>0)
                {
                    float firstPositionBuffer=(ItemCellList[0].transform as RectTransform).anchoredPosition.x;
                    
                    for(int _index=0;_index<ItemCellList.Count;_index++)
                    {
                        var cellTransform=ItemCellList[_index].transform;
                        cellTransform.DOPause();
                        cellTransform.DOScale(Vector3.up*sizeList[_index]+Vector3.right*sizeList[_index]+Vector3.forward*sizeList[_index],setSizeTime);

                        var cellRectTransform=(ItemCellList[_index].transform as RectTransform);
                        cellRectTransform.anchoredPosition = Vector2.right*(firstPositionBuffer+sizePositionList[_index]);  //位置
                    }
                }
                #endregion
            }
        }
        #endregion
    }
}
