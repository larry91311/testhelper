using UnityEngine;
using UnityEngine.UI;

namespace UIHelper
{
    public class BannerCell : BaseCell
    {
        public Image cellImage;

        public int modelIndex;

        public override void Initialize (GameObject gameObject)
        {
            base.Initialize (gameObject);
        }

        public override void Inject ( ControlModel cModel )
        {
            base.Inject ( cModel );

            var bannerCellModel = cModel as BannerCellModel;
            LoadSpriteByID ( bannerCellModel.SpriteID, cellImage );
        }

        protected void LoadSpriteByID ( string spriteAddress ,Image imageBuffer)
        {
            // ResourceManager.Instance.LoadSpriteAsync(
            //     spriteAddress,
            //     (sprite, success) =>
            //     {
            //         if (imageBuffer != null)   //異步保險，避免開關視窗後 取用已銷毀的
            //         {
            //             imageBuffer.sprite = sprite;
            //         }
            //     }
            // );

            Sprite loadSprite= Utility.AssetRelate.ResourcesLoadCheckNull<Sprite>(spriteAddress);
            imageBuffer.sprite = loadSprite;
        }

        

    }

    public class BannerCellModel:ControlModel     //BannerCell
    {
        public int Index { get; set; }
        public string SpriteID { get; set; }
    } 
}
