using System.Collections;
using System.Collections.Generic;
using System.Linq;
using LoadDataFromGoogleSheet.Scripts.ScriptableObject;
using UIHelper;
using UnityEngine;

public class LobbyController : MonoBehaviour
{
    //RightCurtain
    public CurtainHelper rightCurtainHelper;   //1.helper class

    public CurtainBarTable curtainBarTable;

    void Start()
    {
        var rightCurtainTableIndexList = new List<string> { "39000006", "39000007", "39000008", "39000009", "39000010" };
        var rightCurtainOrderList = new List<CurtainOrder>();
        for (int _index = 0; _index < rightCurtainTableIndexList.Count; _index++)
        {
            rightCurtainOrderList.Add
            (
                new CurtainOrder
                {
                    ID = rightCurtainTableIndexList[_index],
                    Priority = curtainBarTable.Get(rightCurtainTableIndexList[_index]).Priority,
                }
            );
        }
        var rightOrderedList = rightCurtainOrderList.OrderByDescending(x => x.Priority).ToList();
        for (int _index = 0; _index < rightCurtainTableIndexList.Count; _index++)
        {
            rightCurtainTableIndexList[_index] = rightOrderedList[_index].ID;
        }

        var curtainRowList = CurtainProcessor.GetCurtainBarRowList(curtainBarTable, rightCurtainTableIndexList);
        //RightCurtain流程
        CreateRightCurtainFlow(curtainRowList);
    }

    //RightCurtain
    void CreateRightCurtainFlow(List<CurtainBarInfo> curtainRowList)
    {
        rightCurtainHelper.Initialize();

        var rightHelperModel = new CurtainHelperModel()
        {
            //LogicType 
            LogicSelection = CurtainLogicSelection.ArrowByCalculation,

            //Data 
            CellAmount = curtainRowList.Count,
            ButtonGap = 231.0f,
            RestrictMovingItemNum = 1,
            LayoutDirection = false,
            ButtonDirection = false,
            SpriteAtlasTitle = "Lobby_0001",

            //Action 
            OnButtonClickAction = OnRightCurtainClick,
            OnArrowClickAction = OnRightCurtainArrowClick,

            //EditorMode
            EditorModeActive = false,
        };
        // rightHelperModel.SpriteNameList = new List<string> ();
        // rightHelperModel.SpriteNameList.AddRange ( CurtainStorager.arrowSpriteIDArray );

        rightHelperModel.CurtainRowList = new List<CurtainBarInfo>();
        rightHelperModel.CurtainRowList.AddRange(curtainRowList);

        rightCurtainHelper.Inject(rightHelperModel);         //Inject
    }

    void OnRightCurtainClick(int index)
    {
        switch (index)
        {
            case 0:
                //OnClickMissionButton ();
                break;
            case 1:
                break;
            case 2:
                //OnClickMaskButton();
                break;
            case 3:
                //OnClickFriendButton ();
                break;
            case 4:
                //OnClickChatButton ();
                break;
            default:
                break;
        }
    }

    void OnRightCurtainArrowClick(bool clickState)
    {
        Debug.LogError("RightCurtainArrowClick:" + clickState);
    }
}
