using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using UnityEngine.AddressableAssets;

namespace UIHelper
{
    public class ButtonCell : BaseCell
    {
        //ANCHOR 物件
        /// <summary>
        /// 按鈕
        /// </summary>
        public Button buttonButton;
        
        /// <summary>
        /// 圖片
        /// </summary>
        public Image buttonImage;

        /// <summary>
        /// 文字
        /// </summary>
        public TextMeshProUGUI buttonText;

        //ANCHOR 資料
        /// <summary>
        /// 順序Index
        /// </summary>
        public int buttonIndex;
        
        /// <summary>
        /// 點擊事件
        /// </summary>
        public UnityAction<int> onClickUnityAction;
        public UnityEvent<int> onClickUnityEvent=new UnityEvent<int>();

        public override void Initialize (GameObject gameObject)
        {
            base.Initialize (gameObject);
        }

        public override void Start ()
        {
            
        }

        public void Inject(CurtainButtonModel model)
        {
            //加載圖片
            LoadSprite(buttonImage,model.ButtonSpriteID);

            //加載文字
            buttonText.text=model.Name;
            //設定順序Index
            buttonIndex=model.Index;
            //加載名稱
            gameObject.name=model.ButtonSpriteID;

            //綁定點擊事件
            buttonButton.onClick.AddListener (
                ()=>
                {
                    onClickUnityEvent?.Invoke(buttonIndex);
                });
        }

        
    }

    public class CurtainButtonModel
    {
        public int Index { get; set; }
        public string Name { get; set; } = string.Empty;
        public string ButtonSpriteID { get; set; }
        public int HighlightType { get; set; }
    }
        
}
