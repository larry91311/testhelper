using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UIHelper
{
    public class ArrowCell : BaseCell
    {
        public GameObject arrowGameObject;
        public Image arrowImage;
        public Button arrowButton;

        public RectTransform rectTransform;

        public UnityAction onClickUnityAction;
        public UnityEvent onClickUnityEvent = new UnityEvent();

        public override void Initialize(GameObject gameObject)
        {
            base.Initialize(gameObject);
        }

        public override void Start()
        {

        }

        public void Inject(ArrowModel arrowModel)
        {
            arrowButton.onClick.AddListener(
                () =>
                {
                    onClickUnityEvent?.Invoke();
                    Debug.LogError("Click");
                });

            LoadSpriteByID(arrowModel.ButtonSpriteID, arrowImage);
        }

        protected void LoadSpriteByID(string spriteAddress, Image imageBuffer)
        {
            // ResourceManager.Instance.LoadSpriteAsync(
            //     spriteAddress,
            //     (sprite, success) =>
            //     {
            //         if (imageBuffer != null)   //異步保險，避免開關視窗後 取用已銷毀的
            //         {
            //             imageBuffer.sprite = sprite;
            //         }
            //     }
            // );

            //Addressables.LoadAssetAsync<Sprite> ( spriteAddress ).Completed += asyncOperationHandle =>   //載圖
            //{
            //    if ( imageBuffer != null )   //異步保險，避免開關視窗後 取用已銷毀的
            //    {
            //        imageBuffer.sprite = asyncOperationHandle.Result;
            //    }
            //};
        }
    }

    public class ArrowModel
    {
        public string ButtonSpriteID { get; set; }
    }
}
