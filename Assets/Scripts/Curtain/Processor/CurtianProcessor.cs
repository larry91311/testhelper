using System.Collections.Generic;
using LoadDataFromGoogleSheet.Scripts.ScriptableObject;

public class CurtainProcessor
{
    // public static List<CurtainBarInfo> GetCurtainBarRowList(SkillData skillData, List<string> tableIndexList)
    // {
    //     var curtainRowList = new List<CurtainBarInfo>();
    //     for (int _index = 0; _index < tableIndexList.Count; _index++)
    //     {
    //         curtainRowList.Add(skillData.Get(tableIndexList[_index]));
    //     }
    //     return curtainRowList;
    // }

    public static List<CurtainBarInfo> GetCurtainBarRowList(CurtainBarTable skillData, List<string> orderedIndexList)
    {
        var curtainRowList = new List<CurtainBarInfo>();
        for (int _index = 0; _index < orderedIndexList.Count; _index++)
        {
            curtainRowList.Add(skillData.Get(orderedIndexList[_index]));
        }
        return curtainRowList;
    }
}

