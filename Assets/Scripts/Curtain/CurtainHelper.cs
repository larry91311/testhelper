using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.AddressableAssets;
using DG.Tweening;
using LoadDataFromGoogleSheet.Scripts.ScriptableObject;

namespace UIHelper
{
    //排序用結構
    public class CurtainOrder
    {
        public string ID { get; set; }
        public int Priority { get; set; }
    }

    public enum CurtainLogicSelection
    {
        ArrowAtEnd,
        ArrowByCalculation,
    }

    public class CurtainHelperModel : ControlModel
    {
        public CurtainLogicSelection LogicSelection { get; set; }

        //Data 
        public int Index;
        public int CellAmount { get; set; }
        public float ButtonGap { get; set; }
        public int RestrictMovingItemNum { get; set; }    //回時 上面留幾個的Arrow位置  
        public bool LayoutDirection { get; set; }
        public bool ButtonDirection { get; set; }

        //spriteAtlasTitle
        public string SpriteAtlasTitle { get; set; }

        //Reference
        public Image BackgroundImagePrefab { get; set; }   //背景Prefab
        public GameObject ButtonPrefab { get; set; }     //Cell Prefab
        public GameObject ArrowPrefab { get; set; }

        //Settings
        public GameObject RedHighlightPrefab { get; set; }
        public GameObject BlueHighlightPrefab { get; set; }
        public GameObject CurtainMaskPrefab { get; set; }

        public List<string> SpriteNameList = new List<string>();    //Load

        //Action
        public UnityAction<int> OnButtonClickAction;
        public UnityAction<bool> OnArrowClickAction;

        //EditorMode
        public bool EditorModeActive;

        //CURTAINROW MODEL
        public List<CurtainBarInfo> CurtainRowList { get; set; }
    }


    public class CurtainHelper : BaseHelper
    {
        //ANCHOR 物件
        /// /// <summary>
        /// 捲簾被捲動的子物件Class群
        /// </summary>
        /// <typeparam name="ButtonCell"></typeparam>
        /// <returns></returns>
        public List<ButtonCell> ButtonCellList = new List<ButtonCell>();

        /// <summary>
        /// 捲簾被捲動的子物件GameObject群
        /// </summary>
        /// <typeparam name="GameObject"></typeparam>
        /// <returns></returns>
        public List<GameObject> ButtonCellGameObjectList = new List<GameObject>();

        /// <summary>
        /// 捲簾箭頭物件GameObject
        /// </summary>
        private GameObject ArrowCellBuffer;

        /// <summary>
        /// 捲簾箭頭物件Class
        /// </summary>
        /// <typeparam name="ArrowCell"></typeparam>
        /// <returns></returns>
        private List<ArrowCell> ArrowCellList = new List<ArrowCell>();

        //ANCHOR 設定
        /// <summary>
        /// 子物件數量
        /// </summary>
        private int ButtonAmount = 1;

        /// <summary>
        /// 子物件間距
        /// </summary>
        private float ButtonGap = 231.0f;

        /// <summary>
        /// 子物件版面方向
        /// </summary>
        private bool LayoutDirection = false;

        /// <summary>
        /// 子物件生成方向
        /// </summary>
        private bool ButtonDirection = false;

        /// <summary>
        /// 子物件捲動時間
        /// </summary>
        private float Duration = 0.5f;

        /// <summary>
        /// 限制移動數量
        /// </summary>
        private int RestrictMovingItemNum = 1;

        /// <summary>
        /// 捲動依照邏輯
        /// </summary>
        private CurtainLogicSelection LogicType = CurtainLogicSelection.ArrowByCalculation;
        #region CurtainD:ControlObjects
        //儲存區


        private GameObject curtainMaskBuffer;     //CurtainMask

        private Image backgroundImageBuffer;    //背景

        //Prefab區
        public Image backgroundImagePrefab;   //背景

        public GameObject buttonPrefab;    //Button

        public GameObject arrowPrefab;     //箭頭   

        public GameObject curtainMaskPrefab;    //Mask
        #endregion
        #region CurtainD:Buffers
        private int buttonAmountBuffer;      //Public Buffer
        private float buttonGapBuffer;
        private int restrictMovingItemNumBuffer;  //保留幾個
        #endregion
        #region CurtainD:Local Field
        //方向
        private int buttonAlignDirectionMul = 0;         //直向、橫向用layoutDirection BOOL判斷，排列左到右或右到左之類正負向用buttonAlignDirectionMul 1、-1

        //Arrow位置偏移
        private float tagPosX = 0;
        private float tagPosY = -210f;

        //動畫
        private float initialHeight = 331f;   //Curtain布幕高度動畫用
        private List<Vector2> curtainCellPositionList = new List<Vector2>();   //存計算過的動畫後位置
        public List<Vector2> animationPositionList = new List<Vector2>();   //存計算過的動畫後位置
        private List<Vector2> arrowPositionList = new List<Vector2>();

        //Arrow換圖的圖
        private List<string> spriteNameList = new List<string>();
        private List<Sprite> curtainSpriteList = new List<Sprite>();   //0:Origin，1:ChangeTo

        private string spriteAtlasTitle = string.Empty;

        //狀態改變事件與屬性子
        public UnityEvent hadClickChangeEvent = new UnityEvent();    //CLICK狀態改變時 事件BUFFER
        private bool hadClick = false;    //CLICK狀態
        public bool HadClick      //變動觸發 2.hadClickChangeEvent(UnityEvent)
        {
            get
            {
                return hadClick;
            }
            set
            {
                Debug.LogError("HadClick");
                if (value != hadClick)
                {
                    hadClickChangeEvent?.Invoke();
                }
                hadClick = value;
            }
        }
        #endregion
        #region CurtainD:Inject
        //CURTAINROW CURTAINROWLIST
        //private List<CurtainBarRow> curtainRowList = new List<CurtainBarRow>();
        private UnityAction<int> OnButtonClickAction;

        private UnityEvent<bool> onArrowClickEvent = new UnityEvent<bool>();

        private UnityAction<bool> OnArrowClickAction;

        public static string[] arrowSpriteIDArray = new string[2]
        {
            "Common_0001[CommonImageArrow_0001]",
            "Common_0001[CommonImageArrow_0002]",
        };
        #endregion

        #region Curtain:PublicFunctions(MainLogic)
        public void ChangeHadClickState()
        {
            HadClick = !HadClick;
        }
        #endregion

        #region Curtain:InjectLoop
        public void Inject(ControlModel helperModel)
        {
            var curtainModel = helperModel as CurtainHelperModel;
            //LogicType
            LogicType = curtainModel.LogicSelection;

            //Data 
            ButtonAmount = curtainModel.CellAmount;
            ButtonGap = curtainModel.ButtonGap;
            RestrictMovingItemNum = curtainModel.RestrictMovingItemNum - 1;
            LayoutDirection = curtainModel.LayoutDirection;
            ButtonDirection = curtainModel.ButtonDirection;
            spriteAtlasTitle = curtainModel.SpriteAtlasTitle;

            spriteNameList.AddRange(curtainModel.SpriteNameList);

            //Action
            OnButtonClickAction = curtainModel.OnButtonClickAction;
            OnArrowClickAction = curtainModel.OnArrowClickAction;

            //CURTAINROW ADDRANGE
            //curtainRowList.AddRange(curtainModel.CurtainRowList);

            DestroyCurtain();        //生成流程
            CreateCurtain();
        }

        #endregion
        #region Curtain:CreateDestroyCurtain
        void CreateCurtain()
        {
            switch (LogicType)
            {
                case CurtainLogicSelection.ArrowAtEnd:
                    {
                        Debug.LogError("PresetGameObjectBeforeLogic_LogicOne");
                        PresetGameObjectBeforeLogic_LogicOne();

                        Debug.LogError("PresetValueBeforeLogic_LogicOne");
                        PresetValueBeforeLogic_LogicOne();

                        Debug.LogError("InstantiateButtonLogic_LogicOne");
                        InstantiateButtonLogic_LogicOne();

                        Debug.LogError("InstantiateArrowLogic_LogicOne");
                        InstantiateArrowLogic_LogicOne();

                        Debug.LogError("BindEventLogic_LogicOne");
                        BindEventLogic_LogicOne();

                        Debug.LogError("LoadSprites");
                        LoadSprites
                        (
                            spriteNameList,
                            curtainSpriteList,
                            () =>
                            {
                                ChangeState_LogicOne();
                            }
                        );
                    }
                    break;

                case CurtainLogicSelection.ArrowByCalculation:
                    {
                        Debug.LogError("PresetGameObjectBeforeLogic_LogicTwo");
                        PresetGameObjectBeforeLogic_LogicTwo();

                        Debug.LogError("PresetValueBeforeLogic_LogicTwo");
                        PresetValueBeforeLogic_LogicTwo();

                        Debug.LogError("InstantiateButtonLogic_LogicTwo");
                        InstantiateButtonLogic_LogicTwo();

                        Debug.LogError("InstantiateArrowLogic_LogicTwo");
                        InstantiateArrowLogic_LogicTwo();

                        Debug.LogError("BindEventLogic_LogicTwo");
                        BindEventLogic_LogicTwo();

                        Debug.LogError("LoadSprites");
                        LoadSprites
                        (
                            spriteNameList,
                            curtainSpriteList,
                            () =>
                            {
                                ChangeState_LogicTwo();
                            }
                        );
                    }
                    break;
            }

        }

        void DestroyCurtain()
        {
            // if (createItem == true)
            // {
            //     while (this.transform.childCount > 0)
            //     {
            //         MonoBehaviour.DestroyImmediate(this.transform.GetChild(0).gameObject);
            //     }
            //     ButtonCellList.Clear();
            // }
            // curtainCellPositionList.Clear();

            // if (ArrowCellBuffer != null && createItem == true)
            // {
            //     MonoBehaviour.DestroyImmediate(ArrowCellBuffer);
            // }

            // int listCount=ButtonCellGameObjectList.Count;
            // for(int i=0;i<listCount;i++)
            // {
            //     GameObject.Destroy(ButtonCellGameObjectList[i]);
            // }
            // ButtonCellGameObjectList.Clear();

            // if(ArrowCellBuffer != null)
            // {
            //     GameObject.Destroy(ArrowCellBuffer);
            // }

            // if(curtainMaskBuffer!=null)
            // {
            //     GameObject.Destroy(curtainMaskBuffer);
            // }

            // GameObject.Destroy(backgroundImageBuffer);
        }
        #endregion
        #region Curtain:ControlObjectCycle
        public void Initialize()
        {
            //base.Initialize(objReference);

        }
        #endregion

        #region LoadSprites
        void LoadSprites(List<string> spriteNames, List<Sprite> spriteList, UnityAction delayAction = null)
        {
            spriteList.Clear();

            var loadCount = 0;
            var listCount = spriteNames.Count;
            for (int i = 0; i < listCount; i++)
            {
                spriteList.Add(null);
            }
            for (int _index = 0; _index < listCount; _index++)
            {
                var temp = _index;
                Addressables.LoadAssetAsync<Sprite>(spriteNames[_index]).Completed += asyncOperationHandle =>
                {
                    spriteList[temp] = asyncOperationHandle.Result;

                    loadCount++;

                    if (loadCount == listCount)
                    {
                        if (delayAction != null)
                        {
                            delayAction();
                        }
                    }
                };
            }
        }
        #endregion

        //共用邏輯區
        #region PresetGameObjectBeforeLogic_General
        //流程一
        void SetArrowsParentIndex_General()
        {
            RestrictMovingItemNum = ButtonAmount - 1;        //強制設定Curtain擺在最上層
        }
        #endregion
        #region PresetValueBeforeLogic_General
        //流程一:順序沒差
        void InitialBufferValue_General()    //初始BUFFER
        {
            buttonAmountBuffer = ButtonAmount;
            buttonGapBuffer = ButtonGap;
            restrictMovingItemNumBuffer = RestrictMovingItemNum;
        }

        //流程二:順序沒差
        void SetDirectionValue_General()      //設定方向參數
        {
            buttonAlignDirectionMul = ButtonDirection ? 1 : -1;
        }

        //流程三:順序沒差
        void InitialPositionData_General()    //生成 每格排序 位置
        {
            Vector2 curtainCellPositionBuffer = Vector2.zero;       //一個位置儲存器
            #region 根據方向預存位置BUTTON
            for (int _index = 0; _index < ButtonAmount; _index++)
            {
                //生成方向，正值:左到右，負值:右到左
                if (LayoutDirection == true)
                {
                    curtainCellPositionBuffer.x = buttonAlignDirectionMul * _index * (ButtonGap);   //計算位置
                    curtainCellPositionBuffer.y = 0;
                    curtainCellPositionList.Add(curtainCellPositionBuffer);   //存LIST
                }
                else
                {
                    curtainCellPositionBuffer.x = 0;
                    curtainCellPositionBuffer.y = /*左右*/buttonAlignDirectionMul * _index * (ButtonGap);  //計算位置
                    curtainCellPositionList.Add(curtainCellPositionBuffer);   //存LIST
                }
            }
            #endregion
        }
        #endregion
        #region InstantiateArrowLogic_General
        void CreateArrowCell_General()
        {
            var model = new ArrowModel()
            {
                ButtonSpriteID = arrowSpriteIDArray[1],
            };

            var arrowCell = ArrowCellBuffer.GetComponent<ArrowCell>();

            arrowCell.onClickUnityEvent.AddListener(ChangeHadClickState);
            arrowCell.onClickUnityEvent.AddListener(
                () =>
                {
                    onArrowClickEvent?.Invoke(HadClick);
                }
            );

            arrowCell.Initialize(ArrowCellBuffer);

            arrowCell.Inject(model);

            ArrowCellList.Add(arrowCell);
        }

        #endregion
        #region InstantiateButtonLogic_General
        void CreateButtonCell_General()
        {
            //var buttonData=CurtainStorager.CurtainInfo;
            for (int _index = 0; _index < ButtonAmount; _index++)
            {
                //CURTAINROW GETTEXT
                //var nameString = StringProcessor.GetText(curtainRowList[_index].NameID); // TableGroup.StringTable.Get ( curtainRowList [ _index ].nameID ).text; // Steven 2022-0406-1700

                //var buttonDataListIndex=CurtainStorager.CurtainData.ButtonCellDataList[_index];
                var model = new CurtainButtonModel()
                {
                    // Index = buttonDataListIndex.Index,
                    // Name = buttonDataListIndex.Name,
                    // ButtonSpriteID = buttonDataListIndex.ButtonSpriteID,
                    // HighlightType = buttonDataListIndex.HighlightType,
                    Index = _index,

                    //CURTAINROW TEXT
                    //Name = nameString,

                    //CURTAINROW IMAGE
                    //ButtonSpriteID = string.Format("{0}", curtainRowList[_index].IconName),

                    //CURTAINROW HIGHTLIGHT
                    //HighlightType = curtainRowList[_index].HintType,
                };

                var buttonCell = ButtonCellList[_index];

                if (OnButtonClickAction != null)
                {
                    buttonCell.onClickUnityEvent.AddListener(OnButtonClickAction);
                }

                buttonCell.Initialize(ButtonCellGameObjectList[_index]);

                buttonCell.Inject(model);

                ButtonCellList.Add(buttonCell);
            }
        }
        #endregion
        #region BindEventLogic_General
        void ChangeArrowSprite(Sprite changeSprite)
        {
            ArrowCellList[0].arrowImage.sprite = changeSprite;
        }
        #endregion
        #region ChangeState_General
        #endregion
        #region DetectValueChanged_General
        #endregion

        //空的，方便複製貼上整區
        #region PresetGameObjectBeforeLogic_Template
        protected void PresetGameObjectBeforeLogic_Template()
        {

        }
        #endregion
        #region PresetValueBeforeLogic_Template
        protected void PresetValueBeforeLogic_Template()
        {

        }
        #endregion
        #region InstantiateArrowLogic_Template
        protected void InstantiateArrowLogic_Template()
        {

        }
        #endregion
        #region InstantiateButtonLogic_Template
        protected void InstantiateButtonLogic_Template()
        {

        }
        #endregion
        #region BindEventLogic_Template
        protected void BindEventLogic_Template()
        {

        }
        #endregion
        #region ChangeState_Template

        protected void ChangeState_Template()
        {

        }
        #endregion

        //第一套邏輯組合
        #region PresetGameObjectBeforeLogic_LogicOne
        protected void PresetGameObjectBeforeLogic_LogicOne()
        {
            SetArrowsParentIndex_LogicOne();  //設定Arrows掛在Hierarchy最下個的Index
        }

        //流程一
        void SetArrowsParentIndex_LogicOne()
        {
            RestrictMovingItemNum = ButtonAmount - 1;        //強制設定Curtain擺在最上層
        }
        #endregion
        #region PresetValueBeforeLogic_LogicOne
        protected void PresetValueBeforeLogic_LogicOne()
        {
            InitialBufferValue_General();   //初始對照BUFFER 使用General 
            SetDirectionValue_General();    //初始 方向及順序的乘數 使用General
            InitialPositionData_LogicOne();      //算位置 
        }

        //流程二:順序沒差
        void SetDirectionValue_LogicOne()
        {
            buttonAlignDirectionMul = ButtonDirection ? 1 : -1;
        }

        //流程三:順序沒差
        void InitialPositionData_LogicOne()  //Index*位差(寬高+間距)
        {
            Vector2 curtainCellPositionBuffer = Vector2.zero;       //一個位置儲存器
            #region 根據方向預存位置BUTTON
            for (int _index = 0; _index < ButtonAmount; _index++)
            {
                //生成方向，正值:左到右，負值:右到左
                if (LayoutDirection == true)
                {
                    curtainCellPositionBuffer.x = buttonAlignDirectionMul * _index * (ButtonGap);   //計算位置
                    curtainCellPositionBuffer.y = 0;
                    curtainCellPositionList.Add(curtainCellPositionBuffer);   //存LIST
                }
                else
                {
                    curtainCellPositionBuffer.x = 0;
                    curtainCellPositionBuffer.y = /*左右*/buttonAlignDirectionMul * _index * (ButtonGap);  //計算位置
                    curtainCellPositionList.Add(curtainCellPositionBuffer);   //存LIST
                }
            }
            #endregion
        }

        #endregion
        #region InstantiateButtonLogic_LogicOne    //邏輯一先生成Button再生成Arrow
        protected void InstantiateButtonLogic_LogicOne()
        {
            InstanCurtainCellsForLoop_LogicOne();   //生成Button
            CreateButtonCell_General();
        }

        //流程一
        void InstanCurtainCellsForLoop_LogicOne()     //生N個 生背景圖 階層反向 生Mask 存新動畫位置
        {

            for (int _index = 0; _index < ButtonAmount; _index++)
            {
                #region 新-生成流程
                //生成
                GameObject newButton = GameObject.Instantiate(buttonPrefab, GameObj.transform);    //生成Button GameObject

                //開
                newButton.gameObject.SetActive(true);

                //設Sibling顯示階層
                newButton.transform.SetAsLastSibling();

                //設名
                newButton.name = _index.ToString();

                //最後一個擺背景圖
                if (_index == ButtonAmount - 1)
                {
                    backgroundImageBuffer = GameObject.Instantiate(backgroundImagePrefab, GameObj.transform);   //生

                    var backgroundRectTransform = backgroundImageBuffer.transform as RectTransform;   //X歸零
                    backgroundRectTransform.anchoredPosition = Vector2.right * 0 + Vector2.up * backgroundRectTransform.anchoredPosition.y;

                    backgroundImageBuffer.transform.SetAsFirstSibling();
                }

                ButtonCellGameObjectList.Add(newButton);
                #endregion
            }

            curtainMaskBuffer = GameObject.Instantiate(curtainMaskPrefab, GameObj.transform);   //生成CurtainMask

            var backImageRectTransform = backgroundImageBuffer.transform as RectTransform;   //取捲簾長度

            var curtainMaskRectTransform = curtainMaskBuffer.transform as RectTransform;    //等同捲簾長度(第一階段)+額外長度(變第二階段捲簾長度)
            curtainMaskRectTransform.sizeDelta = Vector2.right * backImageRectTransform.rect.width + Vector2.up * (backImageRectTransform.rect.height + ((ButtonAmount - 1) * (ButtonGap)));
            curtainMaskRectTransform.anchoredPosition = Vector2.zero;
            curtainMaskRectTransform.anchoredPosition = Vector2.up * (-0.5f * (ButtonAmount - 1) * (ButtonGap) - 1.0f * (ButtonGap) - 65.0f);

            for (int _index = 0; _index < ButtonAmount; _index++)
            {
                var cellRectTransform = ((RectTransform)ButtonCellGameObjectList[_index].transform);          //每個Cell第二階段位置設定
                cellRectTransform.anchoredPosition = Vector2.right * curtainCellPositionList[_index].x + Vector2.up * curtainCellPositionList[_index].y;

                if (_index != 0)
                {
                    ButtonCellGameObjectList[_index].transform.SetParent(curtainMaskBuffer.transform);     //第一個外 Cell設父到Mask下
                }
            }
            for (int _index = 0; _index < ButtonAmount; _index++)    //取動畫位置
            {
                if (_index == 0)
                {
                    if (ButtonCellGameObjectList.Count > 1)
                    {
                        var cellRectTransform = ((RectTransform)ButtonCellGameObjectList[1].transform);      //第一個位置為第二個位置+間距=第一個位置    
                        var animationPos = Vector2.right * cellRectTransform.anchoredPosition.x + Vector2.up * (cellRectTransform.anchoredPosition.y + (ButtonGap));
                        animationPositionList.Add(animationPos);
                    }
                }
                else
                {
                    var cellRectTransform = ((RectTransform)ButtonCellGameObjectList[_index].transform);     //其他為自身位置
                    var animationPos = Vector2.right * cellRectTransform.anchoredPosition.x + Vector2.up * (cellRectTransform.anchoredPosition.y);
                    animationPositionList.Add(animationPos);
                }
            }

        }
        #endregion
        #region InstantiateArrowLogic_LogicOne
        protected void InstantiateArrowLogic_LogicOne()
        {
            InstanArrow_LogicOne();    //生成Arrow
            CreateArrowCell_General();
        }

        //流程一
        void InstanArrow_LogicOne()   //生Arrow 掛最後一個
        {

            #region 新-生成流程
            //生成
            GameObject newArrow = GameObject.Instantiate(arrowPrefab, ButtonCellGameObjectList[ButtonCellList.Count - 1].transform);    //生成GameObject

            //生成後基礎設定
            //InstanCurtainsCurtainAfterInstanLogic_GameObject(newArrow);
            //開
            newArrow.gameObject.SetActive(true);

            //設名
            newArrow.name = "Arrow";

            //設定RECTTRANSFORM
            var arrowTransform = newArrow.transform as RectTransform;
            arrowTransform.anchoredPosition = Vector2.right * tagPosX + Vector2.up * tagPosY;

            if (OnArrowClickAction != null)
            {
                onArrowClickEvent.AddListener(OnArrowClickAction);
            }
            //Project.Tools.EventListener.Get(newArrow).OnClickDelegate += (PointerEventData eventData) => OnArrowClickBaseEvent_General(eventData);

            //存入資料區
            ArrowCellBuffer = newArrow;
            #endregion

        }
        #endregion
        #region BindEventLogic_LogicOne
        protected void BindEventLogic_LogicOne()
        {
            BindHadClickChangeEvent_LogicOne();   //AddListener收集變動邏輯
        }

        //流程一
        public void BindHadClickChangeEvent_LogicOne()
        {
            hadClickChangeEvent.AddListener(DOCurtainCellChangePositionAnimation_LogicOne);
            hadClickChangeEvent.AddListener(DOBackgroundImageScrollAnimation_LogicOne);
            hadClickChangeEvent.AddListener(DOChangeCurtainSprite_LogicOne);
        }

        void DOCurtainCellChangePositionAnimation_LogicOne()  //綁 N個位移 布幕伸縮 Arrow換圖
        {
            var backgroundImageRectTransform = backgroundImageBuffer.transform as RectTransform;
            if (hadClick == true)
            {
                var first = animationPositionList[0];         //頭
                var seq = DG.Tweening.DOTween.Sequence();       //動畫序列
                for (int _index = 0; _index < ButtonAmount; _index++)        //回
                {
                    var buttonCellRectTransform = ((RectTransform)ButtonCellGameObjectList[_index].transform);
                    buttonCellRectTransform.DOPause();
                    if (_index == 0) continue;
                    seq.Join(buttonCellRectTransform.DOAnchorPos(first, Duration).OnComplete(() =>    //動畫結束後除0全關閉
                    {

                    }));
                }
                var lastCell = ButtonCellGameObjectList[ButtonAmount - 1];
                ((RectTransform)lastCell.transform).DOAnchorPos(animationPositionList[0], Duration);   //最後一個行為不同
            }
            else
            {
                var seq = DG.Tweening.DOTween.Sequence();
                for (int _index = 0; _index < ButtonAmount; _index++)         //下
                {
                    var cell = ButtonCellGameObjectList[_index];
                    var buttonCellRectTransform = ((RectTransform)cell.transform);
                    buttonCellRectTransform.DOPause();
                    if (_index == 0) continue;
                    seq.Join(buttonCellRectTransform.DOAnchorPos(animationPositionList[_index], Duration).OnStart(() =>     //非最後一個行為
                    {

                    }));
                }
                var lastCell = ButtonCellGameObjectList[ButtonAmount - 1];
                ((RectTransform)lastCell.transform).DOAnchorPos(animationPositionList[ButtonAmount - 1], Duration);    //最後一個行為不同

                var buttonEndCell = ButtonCellGameObjectList[ButtonAmount - 1];   //因為掛在最後個，拉回後不能關掉最後一個又要顯示第一個，所以不關掉最後一個但關掉他的組件，使ARROW不會一起被關掉
                buttonEndCell.SetActive(true);           //下時 直接顯示最後一個組件
            }
        }

        void DOBackgroundImageScrollAnimation_LogicOne()
        {
            var backgroundImageRectTransform = backgroundImageBuffer.transform as RectTransform;

            var riato = hadClick ? -1 : 1;
            var sizeDelta = backgroundImageRectTransform.sizeDelta;

            //拉長數值
            var tween = DOTween.To(
                () => initialHeight
                , x =>
                {
                    initialHeight = x;
                    backgroundImageRectTransform.sizeDelta = Vector2.up * x + Vector2.right * sizeDelta.x;    //設定拉長程度
                }, initialHeight + (ButtonAmount - 1) * (ButtonGap) * riato, Duration);
        }

        void DOChangeCurtainSprite_LogicOne()
        {
            if (hadClick == false)
            {
                // UpdateManager.Instance.Delay(() =>
                // {
                //     //changeArrowSpriteEvent?.Invoke(curtainSpriteList[1]);
                //     ChangeArrowSprite(curtainSpriteList[1]);
                // }, Duration);
            }
            if (hadClick == true)
            {
                // UpdateManager.Instance.Delay(() =>
                // {
                //     //changeArrowSpriteEvent?.Invoke(curtainSpriteList[0]);
                //     ChangeArrowSprite(curtainSpriteList[0]);
                // }, Duration);
            }
        }


        #endregion
        #region ChangeState_LogicOne
        protected void ChangeState_LogicOne()
        {
            ChangeToHadClickState();    //設定到以點擊的第二階段
        }

        public void ChangeToHadClickState()    //設定成已展開狀態
        {
            hadClick = true;    //變為已點擊

            for (int _index = 0; _index < ButtonAmount; _index++)        //除0全開，變為全開CELL狀態
            {
                if (_index != ButtonAmount - 1)
                {
                    ButtonCellGameObjectList[_index].SetActive(true);
                }
            }

            // for (int _index = 0; _index < ButtonAmount; _index++)         //下，變為全開CELL位置
            // {
            //     var cellRectTransform=((RectTransform)ButtonCellList[_index].transform);
            //     cellRectTransform.anchoredPosition = Vector2.right * curtainCellPositionList[_index].x + Vector2.up * curtainCellPositionList[_index].y;
            // }

            var backgroundImageRectTransform = backgroundImageBuffer.transform as RectTransform;

            initialHeight += ((ButtonAmount - 1) * (ButtonGap));         //設定動畫參數，變為第二階段

            backgroundImageRectTransform.sizeDelta = Vector2.right * backgroundImageRectTransform.sizeDelta.x + Vector2.up * initialHeight;  //背景布幕根據數量拉長，變為全開長度

            ChangeArrowSprite(curtainSpriteList[1]);
        }
        #endregion


        //第二套邏輯組合
        #region PresetGameObjectBeforeLogic_LogicTwo
        protected void PresetGameObjectBeforeLogic_LogicTwo()
        {
            //SetArrowsParentIndex_General();    //強設掛在最末Index，不論數量
        }
        #endregion
        #region PresetValueBeforeLogic_LogicTwo
        protected void PresetValueBeforeLogic_LogicTwo()
        {
            InitialBufferValue_General();   //初始對照BUFFER   
            SetDirectionValue_General();    //初始LAYOUT方向及BUTTON生成方向 乘數
            InitialPositionData_General();      //算位置
            InitialArrowPositionData_LogicTwo();
        }

        void InitialArrowPositionData_LogicTwo()
        {
            var initalArrowPosition = Vector2.right * tagPosX + Vector2.up * (tagPosY - (RestrictMovingItemNum) * (ButtonGap));
            arrowPositionList.Add(initalArrowPosition);

            var endArrowPosition = Vector2.right * tagPosX + Vector2.up * (tagPosY - (ButtonAmount - 1) * (ButtonGap));
            arrowPositionList.Add(endArrowPosition);
        }
        #endregion
        #region InstantiateArrowLogic_LogicTwo
        protected void InstantiateArrowLogic_LogicTwo()
        {
            InstanArrow_LogicTwo();    //生成Arrow
            CreateArrowCell_General();
        }

        //流程一
        void InstanArrow_LogicTwo()     //生Arrow 在最後一個位置 偏移量
        {

            #region 新-生成流程
            //生成
            GameObject newArrow = GameObject.Instantiate(arrowPrefab, GameObj.transform);    //生成GameObject

            //生成後基礎設定
            //InstanCurtainsCurtainAfterInstanLogic_GameObject(newArrow);
            //開
            newArrow.gameObject.SetActive(true);

            //設名
            newArrow.name = "Arrow";

            //特定設定
            //InstanCurtainsCurtainSetValue(newArrow);
            //設定RECTTRANSFORM
            var arrowTransform = newArrow.transform as RectTransform;
            arrowTransform.anchoredPosition = Vector2.right * tagPosX + Vector2.up * (tagPosY - (ButtonAmount - 1) * (ButtonGap));    //末尾位置

            if (OnArrowClickAction != null)
            {
                onArrowClickEvent.AddListener(OnArrowClickAction);
            }
            // Project.Tools.EventListener.Get(newArrow).OnClickDelegate += (PointerEventData eventData) => OnArrowClickBaseEvent_General(eventData);

            //存入資料區
            ArrowCellBuffer = newArrow;
            #endregion

        }
        #endregion
        #region InstantiateButtonLogic_LogicTwo
        protected void InstantiateButtonLogic_LogicTwo()
        {
            InstanCurtainCellsForLoop_LogicTwo();   //生成Button
            CreateButtonCell_General();
        }

        void InstanCurtainCellsForLoop_LogicTwo()  //生N個 生背景圖 生Mask 取新動畫位置
        {

            for (int _index = 0; _index < ButtonAmount; _index++)
            {
                #region 新-生成流程
                //生成
                GameObject newButton = GameObject.Instantiate(buttonPrefab, GameObj.transform);    //生成Button GameObject

                //生成後基礎設定
                //InstanCurtainItemsAfterInstanLogic_GameObject(newButton, _index);
                //開
                newButton.gameObject.SetActive(true);

                //設Sibling顯示階層
                newButton.transform.SetAsLastSibling();

                //設名
                newButton.name = _index.ToString();

                //最後一個擺背景圖
                if (_index == ButtonAmount - 1)
                {
                    backgroundImageBuffer = GameObject.Instantiate(backgroundImagePrefab, GameObj.transform);   //生

                    var backgroundRectTransform = backgroundImageBuffer.transform as RectTransform;   //X歸零
                    backgroundRectTransform.anchoredPosition = Vector2.right * 0 + Vector2.up * backgroundRectTransform.anchoredPosition.y;

                    backgroundImageBuffer.transform.SetAsFirstSibling();
                }

                ButtonCellGameObjectList.Add(newButton);

                ButtonCellList.Add(newButton.GetComponent<ButtonCell>());
                #endregion
            }

            curtainMaskBuffer = GameObject.Instantiate(curtainMaskPrefab, GameObj.transform);   //生成CurtainMask

            var backImageRectTransform = backgroundImageBuffer.transform as RectTransform;   //取捲簾長度

            var curtainMaskRectTransform = curtainMaskBuffer.transform as RectTransform;    //等同捲簾長度(第一階段)+額外長度(變第二階段捲簾長度)

            //設定Mask位置
            curtainMaskRectTransform.sizeDelta = Vector2.right * backImageRectTransform.rect.width + Vector2.up * (backImageRectTransform.rect.height + ((ButtonAmount - 1) * (ButtonGap)));
            curtainMaskRectTransform.anchoredPosition = Vector2.zero;
            curtainMaskRectTransform.anchoredPosition = Vector2.up * (-0.5f * (ButtonAmount - 1) * (ButtonGap) - 1.0f * (ButtonGap) - 65.0f);     //往下一半(PIVOT)往下1(第一個不遮)往下65(微調)

            for (int _index = ButtonAmount - 1; _index >= 0; _index--)
            {
                var cellRectTransform = ((RectTransform)ButtonCellGameObjectList[_index].transform);          //每個Cell第二階段位置設定
                cellRectTransform.anchoredPosition = Vector2.right * curtainCellPositionList[_index].x + Vector2.up * curtainCellPositionList[_index].y;

                if (_index != 0)
                {
                    ButtonCellGameObjectList[_index].transform.SetParent(curtainMaskBuffer.transform);     //第一個外 Cell設父到Mask下
                }
            }
            for (int _index = 0; _index < ButtonAmount; _index++)    //取動畫位置
            {
                if (_index == 0)
                {
                    if (ButtonAmount > 1)
                    {
                        var cellRectTransform = ((RectTransform)ButtonCellGameObjectList[1].transform);      //第一個位置為第二個位置+間距=第一個位置    
                        var animationPos = Vector2.right * cellRectTransform.anchoredPosition.x + Vector2.up * (cellRectTransform.anchoredPosition.y + (ButtonGap));
                        animationPositionList.Add(animationPos);
                    }
                    else
                    {
                        animationPositionList.Add(ButtonCellGameObjectList[0].transform.localPosition);  //其實用不到，但其他地方有取
                    }
                }
                else
                {
                    var cellRectTransform = ((RectTransform)ButtonCellGameObjectList[_index].transform);     //其他為自身位置
                    var animationPos = Vector2.right * cellRectTransform.anchoredPosition.x + Vector2.up * (cellRectTransform.anchoredPosition.y);
                    animationPositionList.Add(animationPos);
                }
            }


        }
        #endregion
        #region BindEventLogic_LogicTwo
        protected void BindEventLogic_LogicTwo()
        {
            BindHadClickChangeEvent_LogicTwo();   //AddListener收集變動邏輯
        }

        //綁定
        public void BindHadClickChangeEvent_LogicTwo()    //綁 N個位移 布幕伸縮 Arrow換圖 Arrow位移 
        {
            hadClickChangeEvent.AddListener(DOCurtainCellChangePositionAnimation_LogicTwo);
            hadClickChangeEvent.AddListener(DOBackgroundImageScrollAnimation_LogicTwo);
            hadClickChangeEvent.AddListener(DOChangeCurtainSprite_LogicTwo);
            hadClickChangeEvent.AddListener(DOArrowChangePositionAnimation_LogicTwo);
        }

        void DOCurtainCellChangePositionAnimation_LogicTwo()
        {
            var backgroundImageRectTransform = backgroundImageBuffer.transform as RectTransform;
            if (hadClick == true)
            {
                var first = animationPositionList[0];         //頭
                var seq = DG.Tweening.DOTween.Sequence();       //動畫序列
                for (int _index = 0; _index < ButtonAmount; _index++)        //回
                {
                    var buttonCellRectTransform = ((RectTransform)ButtonCellGameObjectList[_index].transform);
                    buttonCellRectTransform.DOPause();

                    //不進行動畫continue
                    switch (RestrictMovingItemNum)
                    {
                        case -1:
                            {

                            }
                            break;

                        case 0:
                            {
                                if (_index == 0) continue;
                            }
                            break;

                        case 1:
                            {
                                if (_index == 0 || _index == 1) continue;
                            }
                            break;

                        case 2:
                            {
                                if (_index == 0 || _index == 1 || _index == 2) continue;
                            }
                            break;

                        case 3:
                            {
                                if (_index == 0 || _index == 1 || _index == 2 || _index == 3) continue;
                            }
                            break;

                        case 4:
                            {
                                if (_index == 0 || _index == 1 || _index == 2 || _index == 3 || _index == 4) continue;
                            }
                            break;
                    }
                    seq.Join(buttonCellRectTransform.DOAnchorPos(first, Duration));
                }
            }
            else
            {
                var seq = DG.Tweening.DOTween.Sequence();
                for (int _index = 0; _index < ButtonAmount; _index++)         //下
                {
                    var cell = ButtonCellGameObjectList[_index];
                    var buttonCellRectTransform = ((RectTransform)cell.transform);
                    buttonCellRectTransform.DOPause();

                    //不進行動畫continue
                    switch (RestrictMovingItemNum)
                    {
                        case -1:
                            {

                            }
                            break;

                        case 0:
                            {
                                if (_index == 0) continue;
                            }
                            break;

                        case 1:
                            {
                                if (_index == 0 || _index == 1) continue;
                            }
                            break;

                        case 2:
                            {
                                if (_index == 0 || _index == 1 || _index == 2) continue;
                            }
                            break;

                        case 3:
                            {
                                if (_index == 0 || _index == 1 || _index == 2 || _index == 3) continue;
                            }
                            break;

                        case 4:
                            {
                                if (_index == 0 || _index == 1 || _index == 2 || _index == 3 || _index == 4) continue;
                            }
                            break;
                    }
                    seq.Join(buttonCellRectTransform.DOAnchorPos(animationPositionList[_index], Duration));
                }
            }
        }

        void DOBackgroundImageScrollAnimation_LogicTwo()
        {
            var backgroundImageRectTransform = backgroundImageBuffer.transform as RectTransform;

            var riato = hadClick ? -1 : 1;
            var sizeDelta = backgroundImageRectTransform.sizeDelta;

            //拉長數值
            var tween = DOTween.To(
                () => initialHeight
                , x =>
                {
                    initialHeight = x;
                    backgroundImageRectTransform.sizeDelta = Vector2.up * x + Vector2.right * sizeDelta.x;    //設定拉長程度
                }, initialHeight + (ButtonAmount - 1 - RestrictMovingItemNum) * (ButtonGap) * riato, Duration);
        }

        void DOChangeCurtainSprite_LogicTwo()
        {
            if (hadClick == false)
            {
                // UpdateManager.Instance.Delay(() =>
                // {
                //     ChangeArrowSprite(curtainSpriteList[1]);
                // }, Duration);
            }
            if (hadClick == true)
            {
                // UpdateManager.Instance.Delay(() =>
                // {
                //     ChangeArrowSprite(curtainSpriteList[0]);
                // }, Duration);
            }
        }

        void DOArrowChangePositionAnimation_LogicTwo()
        {
            if (hadClick == true)
            {
                var arrowRectTransform = ArrowCellBuffer.transform as RectTransform;
                arrowRectTransform.DOAnchorPos(arrowPositionList[0], Duration);
            }
            else
            {
                var arrowRectTransform = ArrowCellBuffer.transform as RectTransform;
                arrowRectTransform.DOAnchorPos(arrowPositionList[1], Duration);
            }
        }
        #endregion
        #region ChangeState_LogicTwo
        protected void ChangeState_LogicTwo()
        {
            ChangeToHadClickState_LogicTwo();    //設定到以點擊的第二階段
        }

        public void ChangeToHadClickState_LogicTwo()    //設定成已展開狀態
        {
            hadClick = true;    //變為已點擊

            for (int _index = 0; _index < ButtonAmount; _index++)        //除0全開，變為全開CELL狀態
            {
                if (_index != ButtonAmount - 1)
                {
                    ButtonCellGameObjectList[_index].SetActive(true);
                }
            }

            // for (int _index = 0; _index < ButtonAmount; _index++)         //下，變為全開CELL位置
            // {
            //     var cellRectTransform=((RectTransform)ButtonCellList[_index].transform);
            //     cellRectTransform.anchoredPosition = Vector2.right * curtainCellPositionList[_index].x + Vector2.up * curtainCellPositionList[_index].y;
            // }

            var backgroundImageRectTransform = backgroundImageBuffer.transform as RectTransform;

            initialHeight += ((ButtonAmount - 1) * (ButtonGap));         //設定動畫參數，變為第二階段

            backgroundImageRectTransform.sizeDelta = Vector2.right * backgroundImageRectTransform.sizeDelta.x + Vector2.up * initialHeight;  //背景布幕根據數量拉長，變為全開長度

            ChangeArrowSprite(curtainSpriteList[1]);
        }
        #endregion



    }
}
