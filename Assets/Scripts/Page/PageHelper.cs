using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;
using UnityEngine.AddressableAssets;

namespace UIHelper
{
    public class PageHelperModel : ControlModel
    {
        //Reference
        public GameObject PageCellGameObject;
        public GameObject TopPageCellGameObject;
        public GameObject BottomPageCellGameObject;

        //Settings
        public List<string> SpriteNameList;    //Load
        public bool LoopActive;
        public bool TwoPageActive;
        public bool FadeAnimationActive;

        //Action
        public UnityAction<int> OnRightClickAction;
        public UnityAction<int> OnLeftClickAction;
        public UnityAction<int> OnPageClickAction;
    }

    public class PageHelper : BaseHelper               
    {
        //ANCHOR 功能
        /// <summary>
        /// 單圖淡出或雙圖淡出開關，true:雙圖，false:單圖
        /// </summary>
        private bool TwoPageActive = true;    

        /// <summary>
        /// 無限循環開關
        /// </summary>
        private bool LoopActive = true;       

        /// <summary>
        /// 淡出或淡出淡入動畫效果開關
        /// </summary>
        private bool FadeAnimation = true;  
        
        //ANCHOR 單圖淡出物件
        [Header("單圖淡出圖片")]
        /// <summary>
        /// 單圖淡出圖片容器物件
        /// </summary>
        [SerializeField]
        private GameObject pageCellGameObject;  //顯示屏 單個容器[Header("3.顯示屏CELL容器(Hie第0個)")]

        /// <summary>
        /// 單圖淡出圖片物件
        /// </summary>
        [SerializeField]
        private Image pageCellImage; 

        /// <summary>
        /// 單圖淡出按鈕物件
        /// </summary>
        [SerializeField]
        private Button pageCellButton;    
        
        //ANCHOR 多圖淡出物件
        [Header("多圖淡出下層圖片")]
        /// <summary>
        /// 多圖淡出下層圖片容器物件
        /// </summary>
        [SerializeField]
        private GameObject pageDownCellGameObject;  //顯示屏 單個容器[Header("5.顯示屏CELL容器Down(Hie第1個)")]

        /// <summary>
        /// 多圖淡出下層圖片物件
        /// </summary>
        [SerializeField]
        private Image pageDownCellImage;

        [Header("多圖淡出上層圖片")]
        /// <summary>
        /// 多圖淡出上層圖片容器物件
        /// </summary>
        [SerializeField]
        private GameObject pageUpCellGameObject;  //顯示屏 單個容器[Header("6.顯示屏CELL容器Up(Hie第2個)")]

        /// <summary>
        /// 多圖淡出上層圖片物件
        /// </summary>
        [SerializeField]
        private Image pageUpCellImage; 

        /// <summary>
        /// 多圖淡出上層按鈕物件
        /// </summary>
        [SerializeField]
        private Button pageUpCellButton;
        
        [Header("左點擊切頁按鈕")]
        /// <summary>
        /// 左點擊切頁按鈕物件
        /// </summary>
        [SerializeField]
        private GameObject leftClick;

        /// <summary>
        /// 左點擊切頁按鈕
        /// </summary>
        [SerializeField]
        private Button leftClickButton;

        /// <summary>
        /// 左按鈕圖片
        /// </summary>
        [SerializeField]
        private Image leftClickImage;

        [Header("右點擊切頁按鈕")]
        /// <summary>
        /// 右按鈕物件
        /// </summary>
        [SerializeField]
        private GameObject rightClick;

        /// <summary>
        /// 右按鈕物件
        /// </summary>
        [SerializeField]
        private Button rightClickButton;

        /// <summary>
        /// 右按鈕圖片
        /// </summary>
        [SerializeField]
        private Image rightClickImage;

        [Header("單圖顯示屏")]
        /// <summary>
        /// 單圖淡出頁面Cell Class
        /// </summary>
        [SerializeField]
        private PageCell pageCell; 

        [Header("多圖顯示屏")]
        /// <summary>
        /// 多圖淡出上層頁面Cell Class
        /// </summary>   
        [SerializeField] 
        private PageCell topPageCell;

        /// <summary>
        /// 多圖淡出下層頁面Cell Class
        /// </summary>
        [SerializeField]
        private PageCell bottomPageCell;
        
        //ANCHOR LocalField
        /// <summary>
        /// 點擊方向參數
        /// </summary>
        private int clickDirection = 0;

        /// <summary>
        /// 顯示頁面圖片名稱List
        /// </summary>
        /// <typeparam name="string"></typeparam>
        /// <returns></returns>
        private List<string> spriteNameList = new List<string> ();     //IDs    

        /// <summary>
        /// 顯示頁面圖片List
        /// </summary>
        /// <typeparam name="Sprite"></typeparam>
        /// <returns></returns>    
        private List<Sprite> spriteList = new List<Sprite> ();   //Sprites

        /// <summary>
        /// 按鈕原始色
        /// </summary>
        /// <returns></returns>
        private Color originColor = new Color ( 1, 1, 1, 1 );   

        /// <summary>
        /// 按鈕但出色
        /// </summary>
        /// <returns></returns>
        private Color fadeColor = new Color ( 1, 1, 1, 0.3176f );   

        /// <summary>
        /// 淡入顏色
        /// </summary>
        /// <returns></returns>
        private Color originImageColor = new Color ( 1, 1, 1, 1 );  

        /// <summary>
        /// 淡出顏色
        /// </summary>
        /// <returns></returns>
        private Color fadeImageColor = new Color ( 1, 1, 1, 0 );  

        /// <summary>
        /// 淡入時間(第二張)，淡入 黑->顯 
        /// </summary>
        private float fadeInImageDuration = 0.3f;  

        /// <summary>
        /// 淡出時間(第一張)，淡出 顯->黑
        /// </summary>
        private float fadeOutImageDuration = 0.3f;   

        /// <summary>
        /// 頁面Index
        /// </summary>
        private int currentIndex = 0;      
        public int CurrentIndex
        {
            get
            {
                return currentIndex;
            }
            set
            {
                if ( LoopActive == true )
                {
                    if ( value > spriteList.Count - 1 )
                    {
                        value = 0;
                    }
                    if ( value < 0 )
                    {
                        value = spriteList.Count - 1;
                    }
                }
                currentIndex = value;

                if ( LoopActive == false )
                {
                    ChangePageWhenMinIndex ();
                    ChangePageWhenMaxIndex ();
                }
                currentIndexChangeEvent?.Invoke ();
            }
        }

        /// <summary>
        /// Index改變時觸發事件
        /// </summary>
        /// <returns></returns>
        private UnityEvent currentIndexChangeEvent = new UnityEvent ();    

        /// <summary>
        /// 右點擊觸發事件
        /// </summary>
        /// <typeparam name="int"></typeparam>
        /// <returns></returns>   
        private UnityEvent<int> onRightClickEvent = new UnityEvent<int> ();

        /// <summary>
        /// 左點擊觸發事件
        /// </summary>
        /// <typeparam name="int"></typeparam>
        /// <returns></returns>
        private UnityEvent<int> onLeftClickEvent = new UnityEvent<int> ();

        /// <summary>
        /// 頁面點擊觸發事件
        /// </summary>
        /// <typeparam name="int"></typeparam>
        /// <returns></returns>
        private UnityEvent<int> onPageClickEvent = new UnityEvent<int> ();

        /// <summary>
        /// 右點擊觸發動作
        /// </summary>
        private UnityAction<int> OnRightClickAction;

        /// <summary>
        /// 左點擊觸發動作
        /// </summary>
        private UnityAction<int> OnLeftClickAction;

        /// <summary>
        /// 頁面點擊觸發動作
        /// </summary>
        private UnityAction<int> OnPageClickAction;

        #region Inject
        public override void Inject ( ControlModel helperModel )
        {
            var pageModel = helperModel as PageHelperModel;

            //功能設定
            LoopActive = pageModel.LoopActive;
            TwoPageActive = pageModel.TwoPageActive;
            FadeAnimation = pageModel.FadeAnimationActive;

            //注入動作
            OnRightClickAction = pageModel.OnRightClickAction;
            OnLeftClickAction = pageModel.OnLeftClickAction;
            OnPageClickAction = pageModel.OnPageClickAction;

            
            for ( int i = 0; i < pageModel.SpriteNameList.Count; i++ )
            {
                spriteNameList.Add ( pageModel.SpriteNameList [ i ] );
            }

            if ( OnPageClickAction != null )
            {
                onPageClickEvent.AddListener ( OnPageClickAction );   //添
            }
            pageCellButton.onClick.AddListener (     //綁
                () =>
                {
                    onPageClickEvent?.Invoke ( currentIndex );
                }
            );
            pageUpCellButton.onClick.AddListener (
                () =>
                {
                    onPageClickEvent?.Invoke ( currentIndex );
                }
            );

            LoadSprites (
                spriteNameList,
                () =>
                {
                    SetPageCellState ();
                    SetPageClickActive ();
                    BindLeftRightClick ();
                    BindCurrentIndexChangeEvent ();

                    pageUpCellImage.sprite = spriteList [ 0 ];    //上 換圖
                    pageCellImage.sprite = spriteList [ 0 ];    //上 換圖
                }
            );

        }
        #endregion
        #region ControlObjectCycle
        public override void Initialize ()
        {
            base.Initialize ();

            
        }
        #endregion

        #region MainFunctions
        void LoadSprites ( List<string> spriteNames, UnityAction delayAction = null )
        {
            var loadCount = 0;
            var listCount = spriteNames.Count;
            for ( int _index = 0; _index < listCount; _index++ )
            {
                spriteList.Add ( null );
            }
            for ( int _index = 0; _index < listCount; _index++ )
            {
                var temp = _index;
                Addressables.LoadAssetAsync<Sprite> ( spriteNames [ _index ] ).Completed += asyncOperationHandle =>
                    {
                        spriteList [ temp ] = asyncOperationHandle.Result;

                        loadCount++;

                        if ( loadCount == listCount )
                        {
                            delayAction ();
                        }
                    };
            }
        }

        void CreatePageCell ()
        {
            if ( TwoPageActive == true )
            {
                CreateTwoPageCell ();
            }
            else
            {
                CreateOnePageCell ();
            }
        }

        void CreateOnePageCell ()
        {
            pageCell = pageCellGameObject.GetComponent<PageCell>();

            pageCell.onClick.AddListener ( OnPageClickAction );

            pageCell.Initialize (pageCellGameObject);

            pageCell.Inject ( new PageCellModel ()
            {
                Index = 0,
                SpriteID = "CharacterImage0001[BattleImageCharacter_33007]"
            } );
        }
        void CreateTwoPageCell ()
        {
            //1
            topPageCell = pageUpCellGameObject.GetComponent<PageCell>();

            topPageCell.onClick.AddListener ( OnPageClickAction );

            topPageCell.Initialize ( pageUpCellGameObject );

            topPageCell.Inject (
                new PageCellModel ()
                {
                    Index = 0,
                    SpriteID = "CharacterImage0001[BattleImageCharacter_33007]"
                }
            );

            //2
            bottomPageCell = pageDownCellGameObject.GetComponent<PageCell>();

            bottomPageCell.Initialize ( pageDownCellGameObject );

            bottomPageCell.Inject ( new PageCellModel ()
            {
                Index = 1,
                SpriteID = "CharacterImage0001[BattleImageCharacter_33007]"
            } );
        }

        void SetPageCellState ()       //控制顯示屏
        {
            pageCellGameObject.SetActive ( false );          //關全
            pageUpCellGameObject.SetActive ( false );
            pageDownCellGameObject.SetActive ( false );

            if ( TwoPageActive == false )      //分流開
            {
                pageCellGameObject.SetActive ( true );
            }
            else
            {
                pageUpCellGameObject.SetActive ( true );
                pageDownCellGameObject.SetActive ( true );
            }
        }
        void SetPageClickActive ()          //打開左右紐Raycast、設定左右紐顏色
        {
            leftClickImage.raycastTarget = true;
            leftClickImage.color = originColor;

            rightClickImage.raycastTarget = true;
            rightClickImage.color = originColor;
        }
        void BindLeftRightClick ()        //綁左右鈕事件
        {
            leftClickButton.onClick.AddListener(MinusIndex); 
            rightClickButton.onClick.AddListener(AddIndex);

            if ( OnRightClickAction != null )
            {
                onRightClickEvent.AddListener ( OnRightClickAction );  //添
            }
            if ( OnLeftClickAction != null )
            {
                onLeftClickEvent.AddListener ( OnLeftClickAction );
            }

            leftClickButton.onClick.AddListener(OnLeftClickBaseEvent); 
            rightClickButton.onClick.AddListener(OnRightClickBaseEvent);
        }
        void BindCurrentIndexChangeEvent ()    //綁Index變化事件
        {
            if ( TwoPageActive == false )         //一
            {
                currentIndexChangeEvent.AddListener ( () => { PageChangeSprite ( currentIndex ); } );
            }
            else                       //二
            {
                currentIndexChangeEvent.AddListener ( () => { TwoPageChangeSprite ( currentIndex ); } );
            }
        }
        #endregion
        #region LogicFunctions
        void PageChangeSprite ( int clickNum )     //Index變化事件邏輯
        {
            if ( spriteList.Count > 0 )
            {
                if ( FadeAnimation == true )
                {
                    pageCellImage.DOPause ();
                    pageCellImage.color = originImageColor;    //第一階段 顯 起始顏色

                    pageCellImage.DOColor ( fadeImageColor, fadeOutImageDuration );  //第一階段 顯->黑

                    FunctionTimer.Create (
                        () =>
                        {
                            pageCellImage.sprite = spriteList [ clickNum ];    //第二階段 換圖

                            pageCellImage.DOPause ();
                            pageCellImage.color = fadeImageColor;    //第二階段 黑 起始顏色

                            pageCellImage.DOColor ( originImageColor, fadeInImageDuration );  //第二階段 黑->顯
                        },
                        fadeOutImageDuration
                    );
                }
                else
                {
                    pageCellImage.sprite = spriteList [ clickNum ];    //換圖
                }
            }
        }
        void TwoPageChangeSprite ( int clickNum )
        {
            if ( spriteList.Count > 0 )
            {
                if ( FadeAnimation == true )
                {
                    int clamp = 0;
                    if ( LoopActive == false )
                    {
                        clamp = Mathf.Clamp ( ( clickNum + clickDirection ), 0, spriteList.Count - 1 );    //無Loop 無過界問題
                    }
                    else
                    {
                        if ( clickNum + clickDirection > spriteList.Count - 1 )      //過界回推
                        {
                            clamp = 0;
                        }
                        else if ( clickNum + clickDirection < 0 )
                        {
                            clamp = spriteList.Count - 1;
                        }
                        else
                        {
                            clamp = Mathf.Clamp ( ( clickNum + clickDirection ), 0, spriteList.Count - 1 );
                        }
                    }

                    pageDownCellImage.color = fadeImageColor;    //黑
                    pageUpCellImage.color = fadeImageColor;    //黑

                    pageDownCellImage.DOPause ();
                    pageDownCellImage.color = originImageColor;    //下 顯

                    pageDownCellImage.DOColor ( fadeImageColor, fadeOutImageDuration );  //下 顯->黑

                    pageUpCellImage.DOPause ();
                    pageUpCellImage.color = fadeImageColor;    //上 黑

                    pageUpCellImage.DOColor ( originImageColor, fadeInImageDuration );  //上 黑->顯

                    pageUpCellImage.sprite = spriteList [ clickNum ];    //上 換圖
                    pageDownCellImage.sprite = spriteList [ clamp ];    //下 換圖


                }
                else
                {
                    pageUpCellImage.sprite = spriteList [ clickNum ];    //換圖
                }
            }
        }
        #endregion 

        #region ChangePageFunctions
        void ChangePageWhenMinIndex ()      //下限:防止INPUT
        {
            if ( currentIndex == 0 )
            {
                //leftPage.SetActive(false);
                leftClickImage.color = fadeColor;
                leftClickImage.raycastTarget = false;
            }
            else //if(leftPage.activeInHierarchy==false)
            {
                //leftPage.SetActive(true);
                leftClickImage.color = originColor;
                leftClickImage.raycastTarget = true;
            }
        }

        void ChangePageWhenMaxIndex ()     //上限:防止INPUT
        {
            if ( currentIndex == spriteList.Count - 1 )
            {
                //rightPage.SetActive(false);
                rightClickImage.color = fadeColor;
                rightClickImage.raycastTarget = false;
            }
            else //if(rightPage.activeInHierarchy==false)
            {
                //rightPage.SetActive(true);
                rightClickImage.color = originColor;
                rightClickImage.raycastTarget = true;
            }
        }
        void AddIndex ()    //右點:INDEX++
        {
            clickDirection = -1;
            CurrentIndex++;
        }
        void MinusIndex ()  //左點:INDEX--
        {
            clickDirection = 1;
            CurrentIndex--;
        }

        void OnRightClickBaseEvent ()
        {
            onRightClickEvent?.Invoke ( CurrentIndex );
        }

        void OnLeftClickBaseEvent ()
        {
            onLeftClickEvent?.Invoke ( CurrentIndex );
        }
        #endregion
    }
}
