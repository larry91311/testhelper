using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections.Generic;

namespace UIHelper
{
    public class PageController : MonoBehaviour
    {
        [SerializeField]
        private PageHelper pageHelper;

        [SerializeField]
        private Button backButton;

        public void Start()
        {
            backButton.onClick.AddListener(OnBtnCloseClick);

            var pageTableIndexList = new List<int>() { 1, 2, 3, 4, 5 };
            CreatePageLoop();
        }

        void CreatePageLoop()
        {
            PageHelperModel helperModel = new PageHelperModel()
            {
                //Settings
                LoopActive = true,
                TwoPageActive = true,
                FadeAnimationActive = true,

                SpriteNameList = new List<string>() { "RedCurtain", "OrangeCurtain", "YellowCurtain", "GreenCurtain" },

                //Action
                OnRightClickAction = OnClickRightChangePageButton,
                OnLeftClickAction = OnClickLeftChangePageButton,
                OnPageClickAction = OnClickPageScreenButton,
            };

            pageHelper.Initialize();

            pageHelper.Inject(helperModel);
        }

        public void OnBtnCloseClick()
        {
            UIManager.Instance.ClosePanel(this.name);
        }

        public void OnClickRightChangePageButton(int index)
        {
            
        }

        public void OnClickLeftChangePageButton(int index)
        {
            
        }

        public void OnClickPageScreenButton(int index)
        {
            
        }
    }
}
