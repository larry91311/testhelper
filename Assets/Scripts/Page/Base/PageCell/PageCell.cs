using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UIHelper
{
    public class PageCellModel     //BannerCell
    {
        public int Index { get; set; }
        public string SpriteID { get; set; }
    }
    public class PageCell : BaseCell
    {
        public Image pageImage;

        public UnityEvent<int> onClick = new UnityEvent<int> ();

        public Action A;

        public override void Initialize (GameObject gameObject)
        {
            base.Initialize (gameObject);
        }

        public override void Inject ( ControlModel cModel )
        {
            base.Inject ( cModel );

        }

        public void Inject(PageCellModel model)
        {
            LoadSpriteByID(model.SpriteID,pageImage);
        }

        protected void LoadSpriteByID ( string spriteAddress ,Image imageBuffer)
        {
            // ResourceManager.Instance.LoadSpriteAsync(
            //     spriteAddress,
            //     (sprite, success) =>
            //     {
            //         if (imageBuffer != null)   //異步保險，避免開關視窗後 取用已銷毀的
            //         {
            //             imageBuffer.sprite = sprite;
            //         }
            //     }
            // );

		    Sprite loadSprite= Utility.AssetRelate.ResourcesLoadCheckNull<Sprite>("Resources/"+name);
        }
    }
}
