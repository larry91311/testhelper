using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace UIHelper
{
    public abstract class BaseCell : MonoBehaviour
    {
        public virtual void Initialize (GameObject gameObject)
        {
            
        }

        public virtual void Inject ( ControlModel cModel )
        {

        }

        public virtual void OnEnable ()
        {

        }

        public virtual void Start ()
        {

        }

        public virtual void Update ()
        {

        }

        public virtual void Refresh ()
        {

        }

        public virtual void Resume ()
        {

        }

        public virtual void OnDisable ()
        {

        }

        public virtual void Dispose ()
        {

        }

        protected void LoadSprite ( Image image, string address )
        {
            // Addressables.LoadAssetAsync<Sprite> ( address ).Completed += asyncOperationHandle =>   //載圖
            // {
            //     if ( image != null )   //異步保險，避免開關視窗後 取用已銷毀的
            //     {
            //         image.sprite = asyncOperationHandle.Result;
            //     }
            // };
        }
    }

    public abstract class CellModel  { }
}
