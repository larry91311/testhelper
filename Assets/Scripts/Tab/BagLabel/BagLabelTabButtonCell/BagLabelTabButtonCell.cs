using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace UIHelper
{
    public class BagLabelTabButtonCell : BaseTabButtonCell
    {
        public GameObject normalGameObject;        //未點擊原本GameObject

        public TextMeshProUGUI normalIconText;     //未點擊原本GameObject 的 Icon文字

        public GameObject pressedGameObject;       //點擊切換GameObject

        public TextMeshProUGUI pressedIconText;    //點擊切換GameObject 的 Icon文字

        public Button button;               //Button

        public override void Initialize(GameObject gameObject)
        {
            button.onClick.AddListener(ClickAction);
        }

        public override void Inject(ControlModel cModel)
        {

            var model = cModel as TabCellModel;

            //LoadButtonSpriteByID(model.ButtonSpriteID);

            normalIconText.text = model.Name;
            pressedIconText.text = model.Name;

            buttonIndex = model.Index;

            //GameObj.name = model.Name;
        }

        public void ControlOpenState(int index)
        {
            if (buttonIndex == index)       //目前點擊
            {
                pressedGameObject.gameObject.SetActive(true);
                normalGameObject.gameObject.SetActive(false);
            }
            else                           //目前不是點擊
            {
                pressedGameObject.gameObject.SetActive(false);
                normalGameObject.gameObject.SetActive(true);
            }
        }
    }

    public class BagLabelCellModel : CellModel
    {

    }
}
