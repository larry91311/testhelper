using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using UnityEngine.AddressableAssets;

namespace UIHelper
{
    public enum TabCellType
    {
        TabCell,    //原生
        BagLabelCell,  //背包Label
    }
    public class BaseTabButtonCell : BaseCell
    {
        //ANCHOR 物件
        /// <summary>
        /// 按鈕
        /// </summary>
        public Button buttonButton;   

        /// <summary>
        /// 圖片
        /// </summary>            
        public Image buttonImage;                 

        //ANCHOR 資料
        /// <summary>
        /// 按鈕點擊動作
        /// </summary>
        /// <typeparam name="int"></typeparam>
        /// <returns></returns>
        public UnityEvent<int> onClickButtonEvent=new UnityEvent<int>();   

        /// <summary>
        /// 按鈕索引編號
        /// </summary>   
        public int buttonIndex;

        public void ClickAction()
        {
            onClickButtonEvent?.Invoke(buttonIndex);
        }

        public override void Initialize(GameObject gamObject)
        {
            base.Initialize(gameObject);

            buttonButton.onClick.AddListener(ClickAction);
        }

        public override void Inject(ControlModel cModel)
        {
            base.Inject(cModel);

            var model = cModel as TabCellModel;

            buttonIndex = model.Index;
        }
    }

    public class TabCellModel : ControlModel
    {
        public int Index { get; set; }
        public string Name { get; set; }
        public string ButtonSpriteID { get; set; }
    }
}
