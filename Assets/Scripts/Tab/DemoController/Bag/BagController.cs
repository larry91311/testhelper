using System.Collections.Generic;
using UnityEngine;
using UIHelper;

public class BagController : MonoBehaviour
{
    public TabHelper itemTypeTabHelper;             //Tab Class

    public GameObject[] controlList;

    // Start is called before the first frame update
    void Start()
    {
        CreateTabLoop_LogicOne();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnBtnCloseClick()
    {
        UIManager.Instance.ClosePanel(this.name);
    }

    //LogicOne
    void CreateTabLoop_LogicOne()
    {
        //helper
        itemTypeTabHelper.Initialize();

        var nameIDList = new List<int>() { 4042, 4043, 4044 };

        var tabHelperModel = new TabHelperModel()
        {
            //Data 
            CellAmount = 3,
            ButtonGap = 334.0f,
            LayoutDirection = true,   //true:橫 false:直
            ButtonDirection = true,

            NameIDList = nameIDList,

            //Ability
            ChangeSprite = false,
            BiggerAnimation = false,
            PositionAnimation = false,

            //Action
            OnButtonClickAction = OnTabClick,

            //TabCellType
            CellType = TabCellType.BagLabelCell,

            //LogicType
            LogicType = TabLogicSelection.GenerateTabCellGameObject,
        };
        tabHelperModel.ControlObjList = new List<GameObject>();   //控制顯示
        tabHelperModel.ControlObjList.AddRange(controlList);

        itemTypeTabHelper.Inject(tabHelperModel);         //Inject
    }

    void OnTabClick(int index)
    {
        switch (index)
        {
            case 0:
                {
                    Debug.LogError("Click" + index);
                }
                break;

            case 1:
                {
                    Debug.LogError("Click" + index);
                }
                break;

            case 2:
                {
                    Debug.LogError("Click" + index);
                }
                break;

            default:
                Debug.LogError("ClickDefault" + index);
                break;
        }
    }
}
