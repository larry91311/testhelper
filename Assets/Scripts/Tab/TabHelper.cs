using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace UIHelper
{
    public enum TabLogicSelection
    {
        GenerateTabCellGameObject,
        SetTabCellGameObject,
    }

    public class TabHelper : SelectHelper
    {
        //ANCHOR 邏輯資料
        /// <summary>
        /// TabCell類型
        /// </summary>
        private TabCellType TabCellType;

        /// <summary>
        /// TabHelper邏輯類型
        /// </summary>
        private TabLogicSelection TabLogicType = TabLogicSelection.GenerateTabCellGameObject;   

        //ANCHOR 物件資料
        /// <summary>
        /// 依據邏輯開開關關物件集合
        /// </summary>
        /// <typeparam name="GameObject"></typeparam>
        /// <returns></returns>
        private List<GameObject> ControlObjList = new List<GameObject>();    //控制元件群

        //ANCHOR 捲動屏需求資料
        /// <summary>
        /// 捲動屏
        /// </summary>
        private ScrollRect scrollRect;

        /// <summary>
        /// 可視按鈕數量
        /// </summary>
        private int ViewButtonAmount;

        /// <summary>
        /// 捲動屏功能開關
        /// </summary>
        private bool ScrollViewActive=false;
        
        //ANCHOR 一般LocalField
        /// <summary>
        /// 圖片地址集合
        /// </summary>
        /// <typeparam name="string"></typeparam>
        /// <returns></returns>
        private List<string> spriteIDList = new List<string>();

        //ANCHOR 捲動屏LocalField
        /// <summary>
        /// 起始位置參數
        /// </summary>
        private float initialOffsetMax;
        
        /// <summary>
        /// Arrow物件
        /// </summary>
        private GameObject arrowGameObject;

        #region Tab:CREATEDESTROYTAB
        void CreateTab()
        {
            switch (TabLogicType)
            {
                case TabLogicSelection.GenerateTabCellGameObject:
                {
                    if (ScrollViewActive == false)
                    {
                        PresetGameObjectBeforeLogic_LogicOne();

                        PresetValueBeforeLogic_LogicOne();

                        InstantiateButtonLogic_LogicOne();

                        BindEventLogic_LogicOne();

                        LoadSpriteList
                        (
                            originSpriteNameList,
                            originList
                        );

                        LoadSpriteList
                        (
                            changeToSpriteNameList,
                            changeToList
                        );
                    }
                    else
                    {
                        PresetGameObjectBeforeLogic_LogicThree();

                        PresetValueBeforeLogic_LogicOne();

                        InstantiateButtonLogic_LogicThree();

                        BindEventLogic_LogicOne();

                        LoadSpriteList
                        (
                            originSpriteNameList,
                            originList
                        );

                        LoadSpriteList
                        (
                            changeToSpriteNameList,
                            changeToList
                        );
                    }

                }
                break;

                case TabLogicSelection.SetTabCellGameObject:
                    {
                        PresetGameObjectBeforeLogic_LogicTwo();

                        PresetValueBeforeLogic_LogicTwo();

                        InstantiateButtonLogic_LogicTwo();

                        BindEventLogic_LogicTwo();

                        LoadSpriteList
                        (
                            originSpriteNameList,
                            originList
                        );

                        LoadSpriteList
                        (
                            changeToSpriteNameList,
                            changeToList
                        );
                    }
                    break;
            }
        }
        #endregion
        #region Tab:ControlObjectCycle
        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update()
        {
            //DetectOverBoarderLogic_LogicThree();
        }


        public override void Dispose()
        {
            
        }
        #endregion
        #region Tab:InjectLoop
        public override void Inject(ControlModel helperModel)
        {
            var tabModel = helperModel as TabHelperModel;
            //Data 
            TabCellType=tabModel.CellType;
            ButtonAmount = tabModel.CellAmount;

            Width=tabModel.Width;
            Height=tabModel.Height;
            ButtonGap = tabModel.ButtonGap;
            LayoutDirection = tabModel.LayoutDirection;
            ButtonDirection = tabModel.ButtonDirection;

            spriteIDList=tabModel.SpriteIDList;

            //Prefab用掛，註解
            //cellPrefab=prefabs[(int)TabCellType];

            ScrollViewActive = tabModel.ScollViewActive;

            //Ability
            //ControlObjState = tabModel.ControlObjState;
            PositionAnimation = tabModel.PositionAnimation;
            BiggerAnimation = tabModel.BiggerAnimation;
            ChangeSprite = tabModel.ChangeSprite;
            

            //Action
            OnButtonClickAction = tabModel.OnButtonClickAction;

            ControlObjList.AddRange(tabModel.ControlObjList);

            if (tabModel.OriginSpriteNameList != null && tabModel.OriginSpriteNameList.Count > 0)
            {
                originSpriteNameList.AddRange(tabModel.OriginSpriteNameList);
            }
            if (tabModel.ChangeToSpriteNameList != null && tabModel.ChangeToSpriteNameList.Count > 0)
            {
                changeToSpriteNameList.AddRange(tabModel.ChangeToSpriteNameList);
            }

            //LogicSelectionType
            TabLogicType = tabModel.LogicType;

            //LogicTwo
            if (TabLogicType == TabLogicSelection.SetTabCellGameObject)
            {
                int listCount = CellGameObjectList.Count;
                for (int _index = 0; _index < listCount; _index++)
                {
                    GameObject.Destroy(CellGameObjectList[_index]);
                }
                CellGameObjectList.Clear();
                CellGameObjectList.AddRange(tabModel.BindObjList);
            }

            //LogicThree
            if (ScrollViewActive == true)//LogicType == LogicSelectionType.LogicThree)
            {
                scrollRect = tabModel.ScrollRect;
                ViewButtonAmount = tabModel.ViewButtonAmount;
                arrowGameObject = tabModel.ArrowGameObject;
            }

            CreateTab();
        }
        #endregion

        #region PresetGameObjectBeforeLogic_General
        #endregion
        #region PresetValueBeforeLogic_General
        #endregion
        #region InstantiateButtonLogic_General
        void CreateTabCell_General()
        {
            for (int _index = 0; _index < ButtonAmount; _index++)
            {
                //var nameString=StringProcessor.GetText(nameIDList[_index]);
                var nameString=string.Empty;
                
                var spriteID=string.Empty;
                if(spriteIDList.Count>0)
                {
                    spriteID=spriteIDList[_index];
                }

                ControlModel model=null;          

                switch (TabCellType)
                {
                    case TabCellType.TabCell:
                    {
                        model = new TabCellModel
                        {
                            Index = _index,//tabRowList[_index].Index,
                            Name = nameString,
                            ButtonSpriteID=spriteID,
                        };
                    }
                    break;  

                    case TabCellType.BagLabelCell:
                    {
                        model = new TabCellModel
                        {
                            Index = _index,//tabRowList[_index].Index,
                            Name = nameString,
                        };
                    }
                    break;

                    default:
                    {
                        UnityEngine.Debug.LogError("CreateClass沒這Type");
                    } 
                    break;
                }     

                //TabCell tabCell = new TabCell();
                BaseTabButtonCell tabCell = CellGameObjectList[_index].GetComponent<BaseTabButtonCell>();           //T

                (tabCell as BaseTabButtonCell).onClickButtonEvent.AddListener(SetHelperIndex);
                if (OnButtonClickAction != null)
                {
                    (tabCell as BaseTabButtonCell).onClickButtonEvent.AddListener(OnButtonClickAction);
                }
                tabCell.Initialize(CellGameObjectList[_index]);
                tabCell.Inject(model);
                CellClassList.Add(tabCell);      
            }
        }
        #endregion
        #region BindEventLogic_General
        void TabReveal_General(int clickNum)
        {
            for (int _index = 0; _index < ControlObjList.Count; _index++)    //關全開一
            {
                ControlObjList[_index].SetActive(clickNum == _index);
            }
        }

        void BindTabLogic()
        {
            currentIndexChangeEvent.AddListener
            (
                () =>
                {
                    TabReveal_General(currentIndex);   
                }
            );
        }
        #endregion
        #region ChangeState_General
        #endregion

        #region PresetGameObjectBeforeLogic_LogicOne
        #endregion
        #region PresetValueBeforeLogic_LogicOne
        #endregion
        #region InstantiateButtonLogic_LogicOne
        
        #endregion

        #region BindEventLogic_LogicOne
        protected override void SetCellSprite(Sprite setSprite, int index)
        {
            var tabCell=CellClassList[index] as BaseTabButtonCell;
            if (tabCell.buttonImage != null)
            {
                tabCell.buttonImage.sprite = setSprite;
            }
            else
            {
                //Debug.LogError("ObjectReference沒綁，是刻意沒綁的");
            }
        }

        #endregion

        #region ChangeState_LogicOne
        void SetCurrentIndex(int index)
        {
            CurrentIndex = index;
        }
        #endregion

        #region PresetGameObjectBeforeLogic_LogicTwo
        #endregion
        #region PresetValueBeforeLogic_LogicTwo
        #endregion 
        #region InstantiateButtonLogic_LogicTwo
        void CreateTabCell_LogicTwo()
        {
            for (int _index = 0; _index < ButtonAmount; _index++)
            {
                // // var model=new CurtainButtonModel()
                // // {
                // //     Index = tabData.IndexList[_index],
                // //     Name = tabData.NameList[_index],
                // //     ButtonSpriteID = tabData.ButtonSpriteIDList[_index],
                // // };

                //var tabCell = new TabCell();
                var tabCell = CellGameObjectList[_index].GetComponent<BaseTabButtonCell>();           //T

                tabCell.onClickButtonEvent.AddListener(SetHelperIndex);
                if (OnButtonClickAction != null)
                {
                    tabCell.onClickButtonEvent.AddListener(OnButtonClickAction);
                }

                tabCell.buttonIndex = _index;    //設定Index

                // var tabCellButton=tabCell.buttonButton;
                // tabCellButton=ButtonCellList[_index].transform.GetChild(0).GetComponent<Button>();    //取得Button Reference
                // tabCellButton.onClick.AddListener (tabCell.ClickAction);

                tabCell.Initialize(CellGameObjectList[_index]);
                //tabCell.Inject(model);

                CellClassList.Add(tabCell);
            }
        }
        #endregion
        #region BindEventLogic_LogicTwo
        
        #endregion
        #region ChangeState_LogicTwo
        #endregion

        #region PresetGameObjectBeforeLogic_Three
        void SetScrollViewLayout()
        {
            var buttonPrefabRectTransform = cellPrefab.transform as RectTransform;
            var prefabWidth = buttonPrefabRectTransform.sizeDelta.x;
            var prefabHeight = buttonPrefabRectTransform.sizeDelta.y;

            var scrollRectWidth = ButtonGap * (ViewButtonAmount - 1) + prefabWidth * 1;
            var scrollRectHeight = prefabHeight;
            var scrollRectTransform = scrollRect.transform as RectTransform;
            scrollRectTransform.sizeDelta = Vector2.right * scrollRectWidth + Vector2.up * scrollRectHeight;  //設定scrollRect

            var contentRectTransform = scrollRect.content.transform as RectTransform;
            //var contentWidth= ButtonGap * (ButtonAmount - 1) + prefabWidth * 0.5f;
            var contentWidth = ButtonGap * (ButtonAmount - 1);
            Debug.LogError("ContentWidth:" + contentWidth / 2);
            contentRectTransform.sizeDelta = Vector2.right * contentWidth * 0.5f + Vector2.up * scrollRectHeight;
            //contentRectTransform.offsetMax = Vector2.right * contentWidth/2 + Vector2.up * contentRectTransform.offsetMax.y;
        }
        #endregion
        #region PresetValueBeforeLogic_Three
        #endregion 
        #region InstantiateButtonLogic_Three
        void InstanTabItemsForLoop_LogicThree()
        {
            var contentRectTransform = scrollRect.content.transform as RectTransform;
            var initialPosition = contentRectTransform.offsetMax.x;
            initialOffsetMax = initialPosition;
            Debug.LogError("OFFMINX:" + contentRectTransform.offsetMin.x);
            Debug.LogError("OFFMAXX:" + contentRectTransform.offsetMax.x);
            Debug.LogError("OFFMINY:" + contentRectTransform.offsetMin.y);
            Debug.LogError("OFFMAXY:" + contentRectTransform.offsetMax.y);

            var buttonPrefabRectTransform = cellPrefab.transform as RectTransform;
            var prefabWidth = buttonPrefabRectTransform.sizeDelta.x;
            for (int i = 0; i < ButtonAmount; i++)
            {
                //生成
                GameObject instanButton = GameObject.Instantiate(cellPrefab, GameObj.transform);

                //開
                instanButton.gameObject.SetActive(true);

                //設名
                instanButton.name = i.ToString();

                #region RectTransform設定
                RectTransform instanRectTran = instanButton.transform as RectTransform;
                //生成方向，正值:左到右，負值:右到左
                if (LayoutDirection == true)
                {
                    var x = ((ButtonGap) * buttonDirectionValue * i + initialPosition * -1) * Vector2.right;
                    instanRectTran.anchoredPosition = x;   //橫
                }
                else
                {
                    var y = (ButtonGap) * buttonDirectionValue * i * Vector2.up;
                    instanRectTran.anchoredPosition = y;   //直
                }
                #endregion

                #region 添加進TabCellList
                //添加進TABCELL LIST
                CellGameObjectList.Add(instanButton);
                #endregion
            }
            var buttonRectTransform = CellGameObjectList[0].transform as RectTransform;
            #region 取得動畫 原點
            if (LayoutDirection == true)
            {
                animationPivotPosition = buttonRectTransform.anchoredPosition.y;
            }
            else
            {
                animationPivotPosition = buttonRectTransform.anchoredPosition.x;
            }
            #endregion
        }
        #endregion
        #region BindEventLogic_Three
        #endregion
        #region ChangeState_Three
        #endregion
        private void DetectOverBoarderLogic_LogicThree()
        {
            // Debug.LogError("DetectOverBoarderLogic_LogicThree");
            // var indexList=new List<int>();
            // for(int i=ButtonAmount-1;i>0;i--)
            // {
            //     var buttonCellTransform=ButtonCellList[i].transform as RectTransform;
            //     if(buttonCellTransform.anchoredPosition.x>0)
            //     {
            //         indexList.Add(i);
            //     }
            // }

            // var stringCC="";
            // for(int i=0;i<indexList.Count;i++)
            // {
            //     stringCC=string.Concat(stringCC,indexList[i]);
            // }
            // Debug.LogError(stringCC);

            var contentRectTransform = scrollRect.content.transform as RectTransform;

            // Debug.LogError("OFFMINX:" + contentRectTransform.offsetMin.x);
            // Debug.LogError("OFFMAXX:" + contentRectTransform.offsetMax.x);
            // Debug.LogError("OFFMINY:" + contentRectTransform.offsetMin.y);
            // Debug.LogError("OFFMAXY:" + contentRectTransform.offsetMax.y);



            var buttonPrefabRectTransform = cellPrefab.transform as RectTransform;
            var prefabWidth = buttonPrefabRectTransform.sizeDelta.x;

            //arrowGameObject.SetActive(-contentRectTransform.offsetMax.x<0-ButtonGap);
            arrowGameObject.SetActive(-contentRectTransform.offsetMax.x < 0 - (0.5 * prefabWidth + 0.5f * ButtonGap));

        }

        #region 架構
        #region Template
        void PresetGameObjectBeforeLogic_Template(){}
        void PresetValueBeforeLogic_Template(){}
        void InstantiateButtonLogic_Template(){}
        void BindEventLogic_Template(){}
        void ChangeState_Template(){}
        #endregion

        #region 邏輯一:還未有TabButton，新生成
        void PresetGameObjectBeforeLogic_LogicOne(){}
        void PresetValueBeforeLogic_LogicOne()
        {
            SetLayoutDirectionIntAndButtonInstanDirectionInt_General();
        }
        void InstantiateButtonLogic_LogicOne()
        {
            InstantiateButtonCellGameObjectInRow();
            CreateTabCell_General();
        }
        void BindEventLogic_LogicOne()
        {
            BindTabLogic();
            BindPositionAnimation();
            BindBiggerAnimation();
            BindChangeSprite();
        }
        void ChangeState_LogicOne()
        {
            SetCurrentIndex(0);
        }
        #endregion

        #region 邏輯二:已經有TabButton，只生成class(例如:已經生成Curtain，要把Curtain的Cell加上Tab)
        void PresetGameObjectBeforeLogic_LogicTwo(){}
        void PresetValueBeforeLogic_LogicTwo()
        {
            SetLayoutDirectionIntAndButtonInstanDirectionInt_General();
        }
        void InstantiateButtonLogic_LogicTwo()
        {
            CreateTabCell_LogicTwo();
        }
        void BindEventLogic_LogicTwo()
        {
            BindTabLogic();
            BindPositionAnimation();
            BindBiggerAnimation();
            BindChangeSprite();
        }
        void ChangeState_LogicTwo(){}
        #endregion

        #region 邏輯三:過界，ScrollView，新生成TabButton
        void PresetGameObjectBeforeLogic_LogicThree()
        {
            SetScrollViewLayout();
        }
        void PresetValueBeforeLogic_LogicThree()
        {
            SetLayoutDirectionIntAndButtonInstanDirectionInt_General();
        }
        void InstantiateButtonLogic_LogicThree()
        {
            InstanTabItemsForLoop_LogicThree();
            CreateTabCell_General();
        }
        void BindEventLogic_LogicThree()
        {
            BindTabLogic();
            BindPositionAnimation();
            BindBiggerAnimation();
            BindChangeSprite();
        }
        void ChangeState_LogicThree(){}
        #endregion
        #endregion
    }

    public class TabHelperModel : ControlModel
    {
        //ParentTransform
        public Transform Transform { get; set; }
        //Data 
        public TabCellType CellType { get; set; }
        public int CellAmount { get; set;}

        public float Width { get; set;} =200;       //寬
        public float Height { get; set;} =200;      //高
        public float ButtonGap { get; set; } 

        public bool LayoutDirection { get; set; } 
        public bool ButtonDirection { get; set; }

        public List<GameObject> ControlObjList { get; set;}= new List<GameObject>();
        public List<GameObject> BindObjList { get; set;}=new List<GameObject>();   //LogicTwo
        public List<string> OriginSpriteNameList { get; set;}=new List<string>();
        public List<string> ChangeToSpriteNameList { get; set;}=new List<string>();
        public List<int> NameIDList { get; set;}=new List<int>();
        public List<string> SpriteIDList { get; set;}=new List<string>();

        //Action
        public UnityAction<int> OnButtonClickAction { get; set;}

        //LogicType
        //public LogicSelectionType LogicType { get; set;}
        public TabLogicSelection LogicType { get; set;}

        //功能
        public bool ControlObjState { get; set;}
        public bool PositionAnimation { get; set;}
        public bool BiggerAnimation { get; set;}
        public bool ChangeSprite { get; set;}

        //LogicThree => bool 
        public bool ScollViewActive { get; set;}
        public ScrollRect ScrollRect { get; set;}
        public int ViewButtonAmount { get; set;}
        public GameObject ArrowGameObject { get; set;}
    }
}
