//using System.Runtime.Intrinsics.Arm.Arm64;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

namespace UIHelper
{
    public enum SelectHelperLogicType
    {
        InstantiateButtonCellGameObjectInRow,
        InstantiateButtonCellGameObjectInTransformArray,
    }

    public class SelectHelper : ShowHelper
    {
        //ANCHOR 邏輯資料
        /// <summary>
        /// SelectCell類型
        /// </summary>
        private SelectCellType SelectCellType;
        
        /// <summary>
        /// SelectHelper邏輯類型
        /// </summary>
        private SelectHelperLogicType SelectLogicType = SelectHelperLogicType.InstantiateButtonCellGameObjectInRow;   //預設一
        
        //ANCHOR 功能資料
        /// <summary>
        /// 位置動畫開關
        /// </summary>
        protected bool PositionAnimation = false;

        /// <summary>
        /// 放大縮小動畫開關
        /// </summary>
        protected bool BiggerAnimation = false;

        /// <summary>
        /// 切換圖片開關
        /// </summary>
        protected bool ChangeSprite = false;

        /// <summary>
        /// 捲動屏邏輯開關
        /// </summary>
        private bool ScrollViewActive;

        //ANCHOR LocalField
        /// <summary>
        /// 動畫起點位置
        /// </summary>
        protected float animationPivotPosition = 0;

        /// <summary>
        /// 動畫移動量
        /// </summary>
        protected float moveAmount = 80.0f;

        /// <summary>
        /// 動畫移動持續時間
        /// </summary>
        protected float duration = 0.5f;

        /// <summary>
        /// 切換後圖片地址List
        /// </summary>
        /// <typeparam name="string"></typeparam>
        /// <returns></returns>
        protected List<string> changeToSpriteNameList = new List<string>();
        protected List<Sprite> changeToList = new List<Sprite>();

        /// <summary>
        /// 大小動畫變大值
        /// </summary>
        protected float biggerScaleValue = 1.5f;

        /// <summary>
        /// 大小動畫時間
        /// </summary>
        protected float biggerTime = 0.5f;

        /// <summary>
        /// 目前索引Index
        /// </summary>
        public int currentIndex = 0;       //目前點擊INDEX
        public int CurrentIndex
        {
            get
            {
                return currentIndex;
            }
            set
            {
                currentIndex = value;
                currentIndexChangeEvent?.Invoke();
            }
        }

        /// <summary>
        /// 目前索引Index Set時事件
        /// </summary>
        /// <returns></returns>
        protected UnityEvent currentIndexChangeEvent = new UnityEvent();        //目前點擊INDEX改動 觸發用事件

        /// <summary>
        /// 按鈕點擊注入事件
        /// </summary>
        protected UnityAction<int> OnButtonClickAction;

        #region Tab:MainLogic
        protected void SetHelperIndex(int index)
        {
            CurrentIndex = index;
        }
        #endregion

        #region Tab:CREATEDESTROYTAB
        void CreateTab()
        {
            switch (SelectLogicType)
            {
                case SelectHelperLogicType.InstantiateButtonCellGameObjectInRow:
                {
                    PresetGameObjectBeforeLogic_InstantiateButtonCellGameObjectInRow();

                    PresetValueBeforeLogic_InstantiateButtonCellGameObjectInRow();

                    InstantiateButtonLogic_InstantiateButtonCellGameObjectInRow();

                    BindEventLogic_InstantiateButtonCellGameObjectInRow();

                    LoadSpriteList
                    (
                        originSpriteNameList,
                        originList
                    );

                    LoadSpriteList
                    (
                        changeToSpriteNameList,
                        changeToList
                    );
                }
                break;

                case SelectHelperLogicType.InstantiateButtonCellGameObjectInTransformArray:
                {
                    PresetGameObjectBeforeLogic_InstantiateButtonCellGameObjectInTransformArray();

                    PresetValueBeforeLogic_InstantiateButtonCellGameObjectInTransformArray();

                    InstantiateButtonLogic_InstantiateButtonCellGameObjectInTransformArray();

                    BindEventLogic_InstantiateButtonCellGameObjectInTransformArray();

                    LoadSpriteList
                    (
                        originSpriteNameList,
                        originList
                    );

                    LoadSpriteList
                    (
                        changeToSpriteNameList,
                        changeToList
                    );
                }
                break;
            }
        }
        #endregion
        #region Tab:ControlObjectCycle
        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update()
        {
            
        }


        public override void Dispose()
        {
            
        }
        #endregion
        #region Tab:InjectLoop
        public override void Inject(ControlModel helperModel)
        {
            var selectHelperModel = helperModel as SelectHelperModel;

            //ANCHOR 注入SelectCell類型
            SelectCellType=selectHelperModel.CellType;

            //ANCHOR 注入是否使用捲動屏邏輯
            ScrollViewActive = selectHelperModel.ScollViewActive;

            //ANCHOR 注入SelectHelper邏輯類型
            SelectLogicType = selectHelperModel.LogicType;

            //ANCHOR 注入SelectHelper數據
            ButtonAmount = selectHelperModel.CellAmount;
            Width=selectHelperModel.Width;                 
            Height=selectHelperModel.Height;
            ButtonGap = selectHelperModel.ButtonGap;
            LayoutDirection = selectHelperModel.LayoutDirection;
            ButtonDirection = selectHelperModel.ButtonDirection;
            
            //ANCHOR 注入功能開關
            PositionAnimation = selectHelperModel.PositionAnimation;
            BiggerAnimation = selectHelperModel.BiggerAnimation;
            ChangeSprite = selectHelperModel.ChangeSprite;

            //ANCHOR 注入Button點擊動作
            OnButtonClickAction = selectHelperModel.OnButtonClickAction;

            if (selectHelperModel.OriginSpriteNameList != null && selectHelperModel.OriginSpriteNameList.Count > 0)
            {
                originSpriteNameList.AddRange(selectHelperModel.OriginSpriteNameList);
            }
            if (selectHelperModel.ChangeToSpriteNameList != null && selectHelperModel.ChangeToSpriteNameList.Count > 0)
            {
                changeToSpriteNameList.AddRange(selectHelperModel.ChangeToSpriteNameList);
            }

            switch(SelectLogicType)
            {
                case SelectHelperLogicType.InstantiateButtonCellGameObjectInRow:
                {

                }
                break;

                case SelectHelperLogicType.InstantiateButtonCellGameObjectInTransformArray:
                {
                    instantiateTransforms=selectHelperModel.InstantiateTransforms;
                }
                break;
            }

            CreateTab();
        }
        #endregion

        #region PresetGameObjectBeforeLogic_Template
        void PresetGameObjectBeforeLogic_Template()
        {

        }
        #endregion
        #region PresetValueBeforeLogic_Template
        void PresetValueBeforeLogic_Template()
        {

        }
        #endregion 
        #region InstantiateButtonLogic_Template
        void InstantiateButtonLogic_Template()
        {

        }
        #endregion
        #region BindEventLogic_Template
        void BindEventLogic_Template()
        {

        }
        #endregion
        #region ChangeState_Template
        void ChangeState_Template()
        {

        }
        #endregion

        #region PresetGameObjectBeforeLogic_General
        #endregion
        #region PresetValueBeforeLogic_General
        #endregion
        #region InstantiateButtonLogic_General
        void CreateSelectCell_General()
        {
            //var tabDataButtonDataList=TabStorager.TabData.ButtonCellDataList;
            for (int _index = 0; _index < ButtonAmount; _index++)
            {   
                var model = new SelectButtonModel()
                {
                    Index = _index,
                    //Name = selectRowList[_index].Name,
                    //ButtonSpriteID = selectRowList[_index].ButtonOriginSpriteID,
                    //RaceSpriteID = selectRowList[_index].RaceSpriteID,
                    //FrameSpriteID = selectRowList[_index].FrameSpriteID,
                };
                if(originSpriteNameList.Count>0)
                {
                    model.ButtonSpriteID =originSpriteNameList[_index];
                }

                // var tabCell = new TabCell();
                //SelectCell cell = new SelectCell();           //T
                BaseSelectButtonCell cell = CellGameObjectList[_index].GetComponent<BaseSelectButtonCell>();           //T

                var selectCell=cell as BaseSelectButtonCell;
                selectCell.onClickButtonEvent.AddListener(SetHelperIndex);       //T轉Select
                if (OnButtonClickAction != null)
                {
                    selectCell.onClickButtonEvent.AddListener(OnButtonClickAction);
                }

                var tabCell=selectCell as BaseSelectButtonCell;     //Select轉T
                tabCell.Initialize(CellGameObjectList[_index]);
                tabCell.Inject(model);

                CellClassList.Add(tabCell);
            }
        }
        #endregion
        #region BindEventLogic_General
        protected void PositionAnimation_General(List<GameObject> cellList, int clickNum, float moveAmount, float duration)
        {
            for (int _index = 0; _index < ButtonAmount; _index++)    //FALSE全TRUE一
            {
                var cellRectTransform = cellList[_index].transform as RectTransform;
                if (_index == clickNum)    //目前點 移出
                {
                    if (LayoutDirection == true)
                    {
                        cellRectTransform.DOAnchorPosY(animationPivotPosition + moveAmount, duration);
                    }
                    else
                    {
                        cellRectTransform.DOAnchorPosX(animationPivotPosition + moveAmount, duration);
                    }
                }
                else                  //其他 移回
                {
                    if (LayoutDirection == true)
                    {
                        cellRectTransform.DOAnchorPosY(animationPivotPosition, duration);
                    }
                    else
                    {
                        cellRectTransform.DOAnchorPosX(animationPivotPosition, duration);
                    }
                }
            }
        }

        protected void ChangeSprite_General(List<GameObject> cellList, int clickNum)
        {
            for (int _index = 0; _index < ButtonAmount; _index++)    //FALSE全TRUE一
            {
                if (_index == clickNum)    //目前點 移出
                {
                    Debug.LogError("changeto:" + changeToList.Count);
                    SetCellSprite(changeToList[_index], _index);
                }
                else
                {
                    SetCellSprite(originList[_index], _index);
                }
            }
        }

        protected void BiggerAnimation_General(List<GameObject> cellList, int clickNum)
        {
            for (int _index = 0; _index < ButtonAmount; _index++)    //FALSE全TRUE一
            {
                var cellTransform = cellList[_index].transform;
                if (_index == clickNum)    //目前點 移出
                {
                    cellTransform.DOScale(biggerScaleValue, biggerTime);
                }
                else
                {
                    cellTransform.DOScale(1, biggerTime);
                }
            }
        }
        #endregion
        #region ChangeState_General
        #endregion

        #region PresetGameObjectBeforeLogic_LogicOne
        
        #endregion
        #region PresetValueBeforeLogic_LogicOne
        
        #endregion
        #region InstantiateButtonLogic_LogicOne
    
        #endregion
        #region BindEventLogic_LogicOne
        protected void BindPositionAnimation()
        {
            if (PositionAnimation == true)
            {
                currentIndexChangeEvent.AddListener
                (
                    () =>
                    {
                        PositionAnimation_General(CellGameObjectList, currentIndex, moveAmount, duration);   //位移動畫
                    }
                );
            }
        }

        protected void BindBiggerAnimation()
        {
            if (BiggerAnimation == true)
            {
                currentIndexChangeEvent.AddListener
                (
                    () =>
                    {
                        BiggerAnimation_General(CellGameObjectList, currentIndex);    //大小動畫
                    }
                );
            }
        }

        protected void BindChangeSprite()
        {
            if (ChangeSprite == true)
            {
                currentIndexChangeEvent.AddListener
                (
                    () =>
                    {
                        ChangeSprite_General(CellGameObjectList, currentIndex);     //換圖
                    }
                );
            }
        }

        protected virtual void SetCellSprite(Sprite sprite, int index)
        {
            var selectCell=CellClassList[index] as BaseSelectButtonCell;
            if (selectCell.ImageState())
            {
                selectCell.SetImage(sprite);
            }
            else
            {
                //Debug.LogError("ObjectReference沒綁，是刻意沒綁的");
            }
        }

        #endregion
        #region ChangeState_LogicOne
        #endregion

        #region 架構
        #region Template
        // void PresetGameObjectBeforeLogic_Template();
        // void PresetValueBeforeLogic_Template();
        // void InstantiateButtonLogic_Template();
        // void BindEventLogic_Template();
        // void ChangeState_Template();
        #endregion

        #region 邏輯一:還未有TabButton，新生成
        void PresetGameObjectBeforeLogic_InstantiateButtonCellGameObjectInRow()
        {

        }

        void PresetValueBeforeLogic_InstantiateButtonCellGameObjectInRow()
        {
            SetLayoutDirectionIntAndButtonInstanDirectionInt_General();
        }

        void InstantiateButtonLogic_InstantiateButtonCellGameObjectInRow()
        {
            InstantiateButtonCellGameObjectInRow();
            CreateSelectCell_General();
        }

        void BindEventLogic_InstantiateButtonCellGameObjectInRow()
        {
            BindPositionAnimation();
            BindBiggerAnimation();
            BindChangeSprite();
        }
        #endregion

        #region 邏輯二:生成在TransformList，新生成
        void PresetGameObjectBeforeLogic_InstantiateButtonCellGameObjectInTransformArray()
        {

        }

        void PresetValueBeforeLogic_InstantiateButtonCellGameObjectInTransformArray()
        {
            SetLayoutDirectionIntAndButtonInstanDirectionInt_General();
        }

        void InstantiateButtonLogic_InstantiateButtonCellGameObjectInTransformArray()
        {
            InstantiateButtonCellGameObjectInTransformArray();
            CreateSelectCell_General();
        }

        void BindEventLogic_InstantiateButtonCellGameObjectInTransformArray()
        {
            BindPositionAnimation();
            BindBiggerAnimation();
            BindChangeSprite();
        }
        #endregion
        #endregion
    }

    public class SelectHelperModel : ControlModel
    {
        //Data 
        public SelectCellType CellType { get; set;}
        public int CellAmount { get; set; }
        public float Width { get; set;} =200;       //寬
        public float Height { get; set;} =200;      //高
        public float ButtonGap { get; set;}
        public bool LayoutDirection { get; set;}
        public bool ButtonDirection { get; set;}

        public List<string> OriginSpriteNameList { get; set;}=new List<string>();
        public List<string> ChangeToSpriteNameList { get; set;}=new List<string>();

        //LogicTwo
        public Transform[] InstantiateTransforms { get; set;}

        //Action
        public UnityAction<int> OnButtonClickAction { get; set;}

        //LogicType
        public SelectHelperLogicType LogicType { get; set;}

        //功能
        public bool ControlObjState { get; set;}
        public bool PositionAnimation { get; set;}
        public bool BiggerAnimation { get; set;}
        public bool ChangeSprite { get; set;}

        //LogicThree => bool 
        public bool ScollViewActive { get; set;}
        public ScrollRect ScrollRect { get; set;}
        public int ViewButtonAmount { get; set;}
        public GameObject ArrowGameObject { get; set;}
    }
}
