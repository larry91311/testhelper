using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using UnityEngine.AddressableAssets;

namespace UIHelper
{
    public enum SelectCellType
    {
        SelectCell,    //原生
        //TestTabCell,  //測試繼承
        StepSelectCell,
        StepSelectThirdStepSelectCell,
        RewardSelectCell,
    }

    public class SelectButtonModel:ControlModel
    {
        public int Index { get; set; }
        public string ButtonSpriteID { get; set; }
    }
    public class BaseSelectButtonCell : BaseCell
    {
        //ANCHOR 物件
        /// <summary>
        /// 按鈕
        /// </summary>
        [SerializeField]
        protected Button button;

        /// <summary>
        /// 圖片
        /// </summary>
        [SerializeField]
        protected Image image;

        //ANCHOR 資料
        /// <summary>
        /// Cell索引編號
        /// </summary>
        protected int index;

        /// <summary>
        /// 按鈕點擊事件
        /// </summary>
        /// <typeparam name="int"></typeparam>
        /// <returns></returns>
        public UnityEvent<int> onClickButtonEvent = new UnityEvent<int> ();  

        protected void ClickAction()
        {
            onClickButtonEvent?.Invoke ( index );
        }

        public override void Initialize ( GameObject gameObject )
        {
            base.Initialize ( gameObject );
            
            button.onClick.AddListener (ClickAction);
        }

        public override void Inject(ControlModel model)
        {
            base.Inject ( model );

            var selectCellModel=model as SelectButtonModel;

            //加載圖片
            LoadButtonSpriteByID(selectCellModel.ButtonSpriteID);
            //加載索引編號
            index=selectCellModel.Index;
        }

        protected void LoadButtonSpriteByID ( string spriteAddress )
        {
            // Addressables.LoadAssetAsync<Sprite>(spriteAddress).Completed += asyncOperationHandle =>   //載圖
            // {
            //     if (image != null)   //異步保險，避免開關視窗後 取用已銷毀的
            //     {
            //         image.sprite = asyncOperationHandle.Result;
            //     }
            // };
        }

        public bool ImageState()
        {
            return (image != null);
        }

        public void SetImage(Sprite sprite)
        {
            image.sprite = sprite;
        }
    }
}
