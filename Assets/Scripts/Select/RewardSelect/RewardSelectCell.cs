using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AddressableAssets;

namespace UIHelper
{
    public class RewardSelectCellModel : ControlModel
    {
        public int Index { get; set; }
        public int EquipID { get; set; }
        public string Name { get; set; }
        public string ButtonSpriteID { get; set; }
        public string RaceSpriteID { get; set; }
        public string FrameSpriteID { get; set; }
        //public UnitCellModel UnitCellModel { get; set; }
    }
    public class RewardSelectCell : BaseSelectButtonCell
    {
        //public UnitCellModel unitCellModel;
        public int ID;
        public Transform cellTransform;

        public override void Initialize(GameObject gameObject)
        {
            base.Initialize(gameObject);
        }

        public override void Inject(ControlModel model)
        {
            base.Inject(model);
            
            var selectCellModel = model as SelectButtonModel;
        }

        public override void Refresh()
        {
            
        }
    }
}
