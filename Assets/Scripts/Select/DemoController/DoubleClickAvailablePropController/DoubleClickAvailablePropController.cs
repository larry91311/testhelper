using UnityEngine;
using System.Collections.Generic;


namespace UIHelper
{
    using System;

    public class DoubleClickAvailablePropController : MonoBehaviour
    {
        //SECTION[epic=PartnerC資料區] RaceFilter
        //!SECTION
        [SerializeField]
        SelectHelper rewardSelectHelper;    

        [SerializeField]
        Transform[] rewardParentTransforms;

        //SECTION[epic=UnAvailablePropC資料區] Model
        //!SECTION
        DoubleClickAvailablePropControllerModel clickAvailableModel=new DoubleClickAvailablePropControllerModel();

        public void Start()
        {
            CreateRewardSelectHelper();
        }

        public void OnBtnCloseClick()
        {
            UIManager.Instance.ClosePanel(this.name);
        }

        void CreateRewardSelectHelper()
        {
            //helper
            rewardSelectHelper.Initialize ();

            var selectHelperModel = new SelectHelperModel()
            {
                //Data 
                CellType = SelectCellType.RewardSelectCell,
                CellAmount = 6,
                ButtonGap = 130.95f,
                LayoutDirection = true,   //true:橫 false:直
                ButtonDirection = true,

                //LogicTwo
                InstantiateTransforms=rewardParentTransforms,

                //Ability
                ChangeSprite = false,
                BiggerAnimation = false,
                PositionAnimation = false,

                //Action
                OnButtonClickAction = OnClickRewardSelectCell,

                //LogicType
                LogicType = SelectHelperLogicType.InstantiateButtonCellGameObjectInTransformArray,
            };

            rewardSelectHelper.Inject(selectHelperModel);
        }

        void OnClickRewardSelectCell(int rewardIndex)
        {
            Debug.LogError("Click:"+rewardIndex);
        }
    }

    public class DoubleClickAvailablePropControllerModel : ControlModel
    {
        
    }
}
