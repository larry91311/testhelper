using System;
using System.Collections;
using System.Collections.Generic;
using ThirdParty.Utilities;
using UnityEngine;

namespace GoogleSheetServiceUtility
{
    public static class GoogleSheetService
    {
        public static void LoadDataArray<T>(string url, Action<T[]> complete)
        {
#if UNITY_EDITOR
            EditorWebRequest.Complete += delegate (string jsonText)
            {
                try
                {
                    var result = JsonHelper.FromJson<T>(jsonText, true);
                    complete.Invoke(result);
                }
                catch (Exception)
                {
                    EditorWebRequest.ClearAction();
                    throw;
                }
            };
            EditorWebRequest.Request(url);
#endif
        }
    }

}

