using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
using GoogleSheetServiceUtility;

namespace LoadDataFromGoogleSheet.Scripts.ScriptableObject
{
    [CreateAssetMenu(fileName = "CurtainBarTable", menuName = "ScriptableObjects/CurtainBarTable", order = 1)]
    public class CurtainBarTable : UnityEngine.ScriptableObject
    {
        #region Private Variables
        [SerializeField]
        [LabelWidth(30)]
        [LabelText("Url:")]
        [BoxGroup("LoadData")]
        private string url = "https://opensheet.vercel.app/1ho_K1u9Q5vMUQBKsaqnn-P9qJ3gaALCq2AKyckqXIHo/CurtainBarTable";

        [SerializeField]
        [TableList]
        private CurtainBarInfo[] curtainBarInfos;
        #endregion


        #region Private Methods
        [Button]
        [BoxGroup("LoadData")]
        private void ParseDataFromGoogleSheet()
        {
            GoogleSheetService.LoadDataArray<CurtainBarInfo>(url, infos => curtainBarInfos = infos);
        }
        #endregion

        #region Public Methods
        public CurtainBarInfo Get(string id)
        {
            return Array.Find(curtainBarInfos, (skillInfo) => skillInfo.UUID == id);
        }

        public CurtainBarInfo[] GetInfos()
        {
            return curtainBarInfos;
        }
        #endregion
    }

    [Serializable]
    internal class SkillInfo
    {
        public string UUID => uuid;
        public string Name => name;
        public string Type => type;
        public int Power => power;
        public int Accuracy => accuracy;
        public int PP => pp;
        public string Description => description;

        [SerializeField]
        private string uuid;
        [SerializeField]
        private string name;
        [SerializeField]
        private string type;
        [SerializeField]
        private int power;
        [SerializeField]
        private int accuracy;
        [SerializeField]
        private int pp;
        [SerializeField]
        private string description;
    }

    [Serializable]
    public class CurtainBarInfo
    {
        public string UUID => uuid;
        public string Name => name;
        public string Number => number;
        public string Description => description;
        public string Image => image;
        public int Priority => priority;
        public string HintType => hintType;
        public string SurviveType => surviveType;
        public string TimeStart => timeStart;
        public string TimeEnd => timeEnd;

        [SerializeField]
        private string uuid;
        [SerializeField]
        private string name;
        [SerializeField]
        private string number;
        [SerializeField]
        private string description;
        [SerializeField]
        private string image;
        [SerializeField]
        private int priority;
        [SerializeField]
        private string hintType;
        [SerializeField]
        private string surviveType;
        [SerializeField]
        private string timeStart;
        [SerializeField]
        private string timeEnd;
    }

}

