using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelMain : MonoBehaviour
{
    public void OnBtnShowBannerControllerClick()
    {
        UIManager.Instance.ShowPanel("BannerController");
    }

    public void OnBtnShowDoubleClickAvailableControllerClick()
    {
        UIManager.Instance.ShowPanel("DoubleClickAvailablePropController");
    }

    public void OnBtnShowBagControllerClick()
    {
        UIManager.Instance.ShowPanel("BagController");
    }

    public void OnBtnShowPageControllerClick()
    {
        UIManager.Instance.ShowPanel("PageController");
    }
}
