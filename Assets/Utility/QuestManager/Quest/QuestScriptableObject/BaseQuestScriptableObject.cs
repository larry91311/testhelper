﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Quest", menuName = "Quest System/Quest")]
public class BaseQuestScriptableObject : ScriptableObject
{
    //ANCHOR 任務資訊
    /// <summary>
    /// 標題
    /// </summary>
    public string title;

    /// <summary>
    /// 說明
    /// </summary>
    public string description;

    /// <summary>
    /// 經驗
    /// </summary>
    public int experienceReward;

    /// <summary>
    /// 金幣
    /// </summary>
    public int goldReward;
    
    /// <summary>
    /// 進度
    /// </summary>
    public int progress;

    public int Progress
    {
        set
        {
            progress = value;

            //目前開啟任務 等於 此任務時，刷新ControllerUI
            var currentQuest=QuestManager.Instance.m_CurrentOpenQuest;
            if(title == currentQuest.title)
            {
                QuestManager.Instance.m_QuestController.UpdateControllerUI(currentQuest);
            }

            //任務達成時開啟CompleteButton
            QuestManager.Instance.m_QuestController.ToggleCompleteButton(IsGoalReached);
        }
        get
        {
            return progress;
        }
    }

    /// <summary>
    /// 目標
    /// </summary>
    public int goal;

    //ANCHOR 任務物件
    /// <summary>
    /// 目標物件圖
    /// </summary>
    public Sprite goalImage;

    /// <summary>
    /// 達成圖
    /// </summary>
    public Sprite goalReachedImage;


    //ANCHOR LocalField
    /// <summary>
    /// 是否達成
    /// </summary>
    public bool isGoalReached;

    public bool IsGoalReached
    {
        get
        {
            return progress >= goal;
        }
    }
    
    //ANCHOR Get
    public string GetTitle()
    {
        return title;
    }
    public string GetDescription()
    {
        return description;
    }
    public int GetExperienceReward()
    {
        return experienceReward;
    }
    public int GetGoldReward()
    {
        return goldReward;
    }

    public Sprite GetGoalImage()
    {
        return goalImage;
    }

    public int GetGoal()
    {
        return goal;
    }

    public int GetProgress()
    {
        return progress;
    }

    public bool GetGoalReached()
    {
        return IsGoalReached;
    }
    
    public Sprite GetGoalReachedImage()
    {
        return goalReachedImage;
    }

    //ANCHOR 任務進度增加
    public void IncreaseQuestProgress(int increase)
    {
        var progressAddIncrease = progress + increase;
        if(progressAddIncrease >= goal)
        {
            progressAddIncrease = goal;
            Progress = goal;
        }
        else
        {
            Progress=progressAddIncrease;
        }

        //檢測 開啟完成任務功能

    }

    public void IncreaseOneQuestProgress()
    {
        IncreaseQuestProgress(1);
    }

    //ANCHOR 任務達成後重置進度
    public void AfterCompleteQuestResetQuestProgress()
    {
        progress = 0;
    }
}
