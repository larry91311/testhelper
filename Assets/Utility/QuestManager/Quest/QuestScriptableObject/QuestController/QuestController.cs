using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class QuestController : MonoBehaviour
{
    public TextMeshProUGUI title;

    public TextMeshProUGUI description;

    public TextMeshProUGUI gold;

    public TextMeshProUGUI experience;

    public Image goalImage;

    public TextMeshProUGUI goal;

    public TextMeshProUGUI progress;

    public GameObject completeButton;

    // Start is called before the first frame update
    void Start()
    {
        var currentQuest = QuestManager.Instance.m_CurrentOpenQuest;
        //刷新整個頁面UI資料
        UpdateControllerUI(currentQuest);

        //關閉完成任務鈕
        ToggleCompleteButton(currentQuest.IsGoalReached);
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// 刷新整個任務UI界面
    /// </summary>
    /// <param name="quest"></param>
    public void UpdateControllerUI(BaseQuestScriptableObject quest)
    {
        title.SetText(quest.GetTitle());
        title.ForceMeshUpdate();

        description.SetText(quest.GetDescription());
        description.ForceMeshUpdate();

        gold.SetText(quest.GetGoldReward().ToString());
        gold.ForceMeshUpdate();

        experience.SetText(quest.GetExperienceReward().ToString());
        experience.ForceMeshUpdate();

        //goalImage.sprite=quest.GetGoalImage();

        goal.SetText(quest.GetGoal().ToString());
        goal.ForceMeshUpdate();

        progress.SetText(quest.GetProgress().ToString());
        progress.ForceMeshUpdate();
    }

    /// <summary>
    /// 開關控制完成任務鈕
    /// </summary>
    /// <param name="name"></param>
    /// <param name="isOn"></param>
    public void ToggleCompleteButton(bool isOn)
    {
        completeButton.SetActive(isOn);
    }

    /// <summary>
    /// 關閉視窗
    /// </summary>
    public void OnBtnCloseClick()
    {
        UIManager.Instance.ClosePanel(this.name);
    }

    /// <summary>
    /// 點擊完成任務鈕
    /// </summary>
    public void OnBtnCompleteQuestClick()
    {
        QuestManager.Instance.PlayerCompleteQuest(title.text);
        QuestManager.Instance.m_CurrentOpenQuest.AfterCompleteQuestResetQuestProgress();
        Debug.LogErrorFormat("Earn {0} gold,Obtain {1} experience", gold, experience);
    }
}
