using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class QuestManager : SerializedMonoBehaviour
{
    public static QuestManager Instance;

    private void Awake()
    {
        Instance = this;
    }
    
    /// <summary>
    /// 玩家擁有的任務
    /// </summary>
    /// <typeparam name="string"></typeparam>
    /// <typeparam name="QuestScriptableObject"></typeparam>
    /// <returns></returns>
    public Dictionary<string, BaseQuestScriptableObject> m_PlayersQuestDictionary = new Dictionary<string, BaseQuestScriptableObject>();
    
    /// <summary>
    /// 系統擁有的任務
    /// </summary>
    /// <typeparam name="string"></typeparam>
    /// <typeparam name="QuestScriptableObject"></typeparam>
    /// <returns></returns>
    public Dictionary<string, BaseQuestScriptableObject> m_SystemsQuestDictionary = new Dictionary<string, BaseQuestScriptableObject>();
    
    public BaseQuestScriptableObject m_CurrentOpenQuest;

    public QuestController m_QuestController;

    bool IsSystemQuestExist(string name)
    {
        return m_SystemsQuestDictionary.ContainsKey(name);
    }

    bool IsPlayerQuestExist(string name)
    {
        return m_PlayersQuestDictionary.ContainsKey(name);
    }

    /// <summary>
    /// 字典內搜尋任務
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public BaseQuestScriptableObject GetQuest(string name)
    {
        if (!IsSystemQuestExist(name))
        {
            Debug.LogErrorFormat("[{0}] does not exist in m_SystemsQuestDictionary, if you want to get, please create and add first!!", name);
            return null;
        }

        return m_SystemsQuestDictionary[name];
    }
    
    /// <summary>
    /// 玩家接收任務
    /// </summary>
    public void PlayerReceiveQuest()
    {
        //System取得任務
        var quest=GetQuest(m_CurrentOpenQuest.title);
        if (quest==null)
        {
            Debug.LogErrorFormat("[{0}] does not exist in m_SystemsQuestDictionary, if you want to get, please create and add first!!", name);
            return;
        }

        //設置目前任務
        m_CurrentOpenQuest=quest;

        //第一次獲得任務添加進PlayerDictionary
        if(quest!=null && !m_PlayersQuestDictionary.ContainsKey(quest.title))
        {
            //第一次添加
            m_PlayersQuestDictionary.Add(m_CurrentOpenQuest.title,quest);
        }
        else
        {
            //純開啟
        }

        //開啟視窗，存取視窗
        UIManager.Instance.ShowPanel(
            "QuestController",
            ()=>
            {
                m_QuestController=UIManager.Instance.GetPanel("QuestController").GetComponent<QuestController>();
            }
        );
    }

    /// <summary>
    /// 玩家完成任務
    /// </summary>
    public void PlayerCompleteQuest(string name)
    {
        //Player任務字典內移除
        m_PlayersQuestDictionary.Remove(name);

        UIManager.Instance.ClosePanel("QuestController");
    }
    
    /// <summary>
    /// 玩家任務進度+1
    /// </summary>
    /// <param name="name"></param>
    public void IncreaseOneQuestProgress(string name)
    {
        if (!IsSystemQuestExist(name))
        {
            Debug.LogErrorFormat("[{0}] does not exist in m_PlayersQuestDictionary, if you want to get, please create and add first!!", name);
            return;
        }

        m_PlayersQuestDictionary[name].IncreaseOneQuestProgress();
    }
    
    /// <summary>
    /// 測試任務進度增加
    /// </summary>
    public void TestIncreaseOneQuestProgress()
    {
        QuestManager.Instance.IncreaseOneQuestProgress("KillOneEnemy");
    }

}
