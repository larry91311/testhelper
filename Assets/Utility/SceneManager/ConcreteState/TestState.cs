using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SceneUtility
{
    public class TestState : ISceneState
    {
        #region Variables
        #endregion

        #region Constructors
        public TestState(SceneStateController Controller) : base(Controller)
        {
            this.StateName = "TestState";
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// 開始
        /// </summary>
        public override void StateBegin()
        {

        }

        /// <summary>
        /// 結束
        /// </summary>
        public override void StateEnd()
        {

        }

        /// <summary>
        /// 更新
        /// </summary>
        public override void StateUpdate()
        {

        }
        #endregion

        #region Private Methods
        #endregion
    }
}
