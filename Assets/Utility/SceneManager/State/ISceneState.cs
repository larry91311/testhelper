using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SceneUtility
{
    public class ISceneState
    {
        #region Variables
        /// <summary>
        /// 狀態名稱
        /// </summary>
        private string m_StateName = "ISceneState";

        public string StateName
        {
            get { return m_StateName; }
            set { m_StateName = value; }
        }

        protected SceneStateController m_Controller = null;
        #endregion

        #region Constructors
        public ISceneState(SceneStateController Controller)
        {
            m_Controller = Controller;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// 開始
        /// </summary>
        public virtual void StateBegin()
        {

        }

        /// <summary>
        /// 結束
        /// </summary>
        public virtual void StateEnd()
        {

        }

        /// <summary>
        /// 更新
        /// </summary>
        public virtual void StateUpdate()
        {

        }

        public override string ToString()
        {
            return string.Format("[I_SceneState:StateName={0}]", StateName);
        }
        #endregion

        #region Private Methods
        #endregion
    }
}
