using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SceneUtility
{
    public class SceneStateController
    {
        #region Variables
        /// <summary>
        /// 目前狀態
        /// </summary>
        private ISceneState m_State;

        private bool m_bRunBegin = false;
        #endregion

        #region Constructors
        public SceneStateController() { }
        #endregion

        #region Public Methods
        /// <summary>
        /// 設定狀態
        /// </summary>
        /// <param name="State"></param>
        /// <param name="LoadSceneName"></param>
        public void SetState(ISceneState State, string LoadSceneName)
        {
            Debug.Log("SetState:" + LoadSceneName);

            m_bRunBegin = false;

            LoadScene(LoadSceneName);

            //通知前一個State結束
            if (m_State != null)
            {
                m_State.StateEnd();
            }

            //設定
            m_State = State;
        }

        public void StateUpdate()
        {
            //是否還在載入
            if (Application.isLoadingLevel)
            {
                return;
            }

            if (m_State != null && m_bRunBegin == false)
            {
                m_State.StateBegin();
                m_bRunBegin = true;
            }

            if (m_State != null)
            {
                m_State.StateUpdate();
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// 載入場景
        /// </summary>
        /// <param name="LoadSceneName"></param>
        private void LoadScene(string LoadSceneName)
        {
            if (LoadSceneName == null || LoadSceneName.Length == 0)
            {
                return;
            }

            SceneManager.LoadScene(LoadSceneName);
        }
        #endregion
    }
}
