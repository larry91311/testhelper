using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace SceneUtility
{
    public class ConcreteISceneStateAutoGenerator_Menu
    {
        [MenuItem("MyTools/ConcreteISceneStateAutoGenerator")]   //%#g:可使用Shift開啟
        public static void ConcreteISceneStateAutoGenerate()
        {
            ConcreteISceneStateAutoGenerator_Editor.LaunchEditor();
        }
    }
}
