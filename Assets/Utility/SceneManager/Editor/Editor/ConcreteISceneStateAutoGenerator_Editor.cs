using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Text;
using System.IO;

namespace SceneUtility
{
    public class ConcreteISceneStateAutoGenerator_Editor : EditorWindow
    {
        #region Variables
        /// <summary>
        /// 新創立concreteClass名稱
        /// </summary>
        string className;

        /// <summary>
        /// 最後一個concreteState名稱
        /// </summary>
        string lastClassName = "BattleState";

        /// <summary>
        /// 整個Template檔的每一行 存List
        /// </summary>
        /// <typeparam name="string"></typeparam>
        /// <returns></returns>
        List<string> lineList = new List<string>();
        #endregion

        #region BuiltIn Methods
        public static void LaunchEditor()
        {
            var win = GetWindow<ConcreteISceneStateAutoGenerator_Editor>("ConcreteISceneStateAutoGenerator");
            win.Show();
        }

        private void OnGUI()
        {
            //最外層直向開始
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.Space();

            //直向第一層
            EditorGUILayout.BeginVertical();
            EditorGUILayout.Space();

            className = EditorGUILayout.TextField("className:", className);
            lastClassName = EditorGUILayout.TextField("lastClassName:", lastClassName);

            if (GUILayout.Button("Auto Generate Code", GUILayout.Height(45), GUILayout.ExpandWidth(true)))
            {
                AutoGenerateCode();
            }

            EditorGUILayout.LabelField(lastClassName);

            //最外層直向結束
            EditorGUILayout.Space();
            EditorGUILayout.EndVertical();

            //最外層直向結束
            EditorGUILayout.Space();
            EditorGUILayout.EndHorizontal();

            Repaint();
        }
        #endregion

        #region Custom Methods
        void AutoGenerateCode()
        {
            //清空每行List Buffer
            lineList.Clear();
            string line = string.Empty;

            //取得template地址、目標地址(自動建立)
            string templateAddress = "Assets/Utility/SceneManager/Editor/Templates/ISceneStateTemplate.txt";
            string targetAddress = "Assets/Utility/SceneManager/ConcreteState/" + className + ".cs";
            string gameLoopAddress = "Assets/Utility/SceneManager/GameLoop/GameLoop.cs";

            //檢查目標檔案存不存在，如果已經有這個ISceneState，中斷執行，跳出警告
            if (System.IO.File.Exists(targetAddress))
            {
                UnityEngine.Debug.LogWarning("ConcreteISceneStateAutoGenerator_Editor break; " + className + ".cs is already Existed");
                return;
            }

            //讀取樣板檔案每行，添加REPLACE後的行進LINELIST
            using (StreamReader sr = new StreamReader(templateAddress))
            {
                while ((line = sr.ReadLine()) != null)
                {
                    lineList.Add(line.Replace("$StartState", className));
                }
            }

            //寫入新創成腳本每行，每行變更為LINELIST內的替換行
            using (StreamWriter sw = new StreamWriter(targetAddress))
            {
                foreach (string eachLine in lineList)
                {
                    sw.WriteLine(eachLine);
                }
            }

            //清空每行List Buffer
            lineList.Clear();
            line = string.Empty;

            //讀取GameLoop檔案每行，添加REPLACE後的行進LINELIST 1
            using (StreamReader sr = new StreamReader(gameLoopAddress))
            {
                while ((line = sr.ReadLine()) != null)
                {
                    lineList.Add(line);

                    //後一行
                    if (line.Contains("m_StateList.Add(new " + lastClassName + "(m_SceneStateController));"))
                    {
                        lineList.Add("        m_StateList.Add(new " + className + "(m_SceneStateController));");   //第二行Tab，空白鍵8格
                    }
                }
            }

            //寫入GameLoop檔案每行，每行變更為LINELIST內的替換行
            using (StreamWriter sw = new StreamWriter(gameLoopAddress))
            {
                foreach (string eachLine in lineList)
                {
                    sw.WriteLine(eachLine);
                }
            }

            //最後一個className=新生成className，用作GameLoop Add判斷
            lastClassName = className;

            AssetDatabase.Refresh();
        }
        #endregion
    }
}
