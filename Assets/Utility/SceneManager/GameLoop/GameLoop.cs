using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SceneUtility;

public class GameLoop : MonoBehaviour
{
    SceneStateController m_SceneStateController = new SceneStateController();

    List<ISceneState> m_StateList = new List<ISceneState>();

    void Awake()
    {
        if (GameObject.Find("GameLoop") == null)
        {
            GameObject.DontDestroyOnLoad(this.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //添加State
        m_StateList.Add(new StartState(m_SceneStateController));
        m_StateList.Add(new MainMenuState(m_SceneStateController));
        m_StateList.Add(new BattleState(m_SceneStateController));
        m_StateList.Add(new TestState(m_SceneStateController));
        m_StateList.Add(new Test1State(m_SceneStateController));


        m_SceneStateController.SetState(m_StateList[1], "MainMenuScene");
    }

    // Update is called once per frame
    void Update()
    {
        m_SceneStateController.StateUpdate();
    }
}
