using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IButtonAlignmentSetting : MonoBehaviour
{
    public virtual void SetButtonAlignment(int i,int rows,int columns,ref float xPos,ref float yPos,Vector2 cellSize,Vector2 spacing,ref int columnCount,ref int rowCount,RectOffset padding,int maxRow,int maxColumn)
    {

    }
}
