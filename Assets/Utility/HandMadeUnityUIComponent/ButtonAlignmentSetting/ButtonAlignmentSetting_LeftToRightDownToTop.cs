using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonAlignmentSetting_LeftToRightDownToTop : IButtonAlignmentSetting
{
    public override void SetButtonAlignment(int i,int rows,int columns,ref float xPos,ref float yPos,Vector2 cellSize,Vector2 spacing,ref int columnCount,ref int rowCount,RectOffset padding,int maxRow,int maxColumn)
    {
        rowCount=i/columns;        //INT除INT後 INT無條件捨去=在第N列(0起)=列COUNT
        columnCount=i%columns;        //INT%INT後 餘數=第N個(0起)=行COUNT

        xPos+= (cellSize.x*columnCount)+/*SPACING*/(spacing.x*columnCount)+padding.left;
        yPos+= (cellSize.y*(maxRow-1-rowCount))+/*SPACING*/(spacing.y*rowCount)+padding.top;
    }
}
