using System.Dynamic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlexibleGridLayout : LayoutGroup
{
    [Header("Data")]
    [Range(1,20)]
    public int rows;
    [Range(1,20)]
    public int columns;
    public Vector2 cellSize;
    #region AddingFeature
    [Header("Spacing")]
    public Vector2 spacing;
    [Header("Type")]
    public FitType fitType;
    public FitType Fittype
    {
        get
        {
            return fitType;
        }
        set
        {
            fitType=value;
            SetRowColumnSetting();
        }
    }
    private FitType fitTypeBuffer;
    public enum FitType
    {
        Uniform,   //GRIDLAYOUT
        Width,    //先平方根 其一再根據另一變動
        Height,   //先平方根 其一再根據另一變動
        FixedRows,    //自定義  其一再根據另一變動
        FixedColumns,  //自定義  其一再根據另一變動
        Custom
    }

    [Header("Type")]
    public ButtonAlign buttonAlignType;
    public ButtonAlign ButtonAlignType
    {
        get
        {
            return buttonAlignType;
        }
        set
        {
            buttonAlignType=value;
            SetButtonAlignmentSetting();
        }
    }
    private ButtonAlign buttonAlignTypeBuffer;
    public enum ButtonAlign
    {
        LeftRightTopDown,
        RightLeftTopDown,
        LeftRightDownTop,
        RightLeftDownTop,
        TopDownLeftRight,
        TopDownRightLeft,
        DownTopLeftRight,
        DownTopRightLeft
    }
    
    public bool fitX;
    public bool fitY;
    
    [Header("Cycle")]
    public IRowColumnSetting rowColumnSetting;
    public IButtonAlignmentSetting buttonAlignmentSetting;
    [Header("StrategyDictionary")]
    public Dictionary<string,IRowColumnSetting> rowColumnSettingDictionary=new Dictionary<string, IRowColumnSetting>();
    public Dictionary<string,IButtonAlignmentSetting> buttonAlignmentSettingDictionary=new Dictionary<string, IButtonAlignmentSetting>();

    [ContextMenu("AddRowColumnSettingDictionary")]
    public void AddRowColumnSettingDictionary()
    {
        rowColumnSettingDictionary=new Dictionary<string, IRowColumnSetting>();
        if(!rowColumnSettingDictionary.ContainsKey(FitType.Uniform.ToString()))
        {
            rowColumnSettingDictionary.Add(FitType.Uniform.ToString(),transform.GetComponent<RowColumnSetting_Uniform>());
        }
        if(!rowColumnSettingDictionary.ContainsKey(FitType.FixedRows.ToString()))
        {
            rowColumnSettingDictionary.Add(FitType.FixedRows.ToString(),transform.GetComponent<RowColumnSetting_FixedRows>());
        }
        if(!rowColumnSettingDictionary.ContainsKey(FitType.FixedColumns.ToString()))
        {
            rowColumnSettingDictionary.Add(FitType.FixedColumns.ToString(),transform.GetComponent<RowColumnSetting_FixedColumns>());
        }
        if(!rowColumnSettingDictionary.ContainsKey(FitType.Custom.ToString()))
        {
            rowColumnSettingDictionary.Add(FitType.Custom.ToString(),transform.GetComponent<RowColumnSetting_Custom>());
        }
        Debug.Log(rowColumnSettingDictionary.Count);


    }

    public void SetRowColumnSetting()
    {
        Debug.Log("Set");
        if(rowColumnSettingDictionary.ContainsKey(fitType.ToString()))
        {
            rowColumnSetting=rowColumnSettingDictionary[fitType.ToString()];
        }
        Debug.Log(rowColumnSettingDictionary.ContainsKey(fitType.ToString()));
    }



    public void AddButtonAlignmentSettingDictionary()
    {
        buttonAlignmentSettingDictionary=new Dictionary<string, IButtonAlignmentSetting>();
        if(!buttonAlignmentSettingDictionary.ContainsKey(FitType.Uniform.ToString()))
        {
            buttonAlignmentSettingDictionary.Add(ButtonAlign.LeftRightTopDown.ToString(),transform.GetComponent<ButtonAlignmentSetting_LeftToRightTopToDown>());
        }
        if(!buttonAlignmentSettingDictionary.ContainsKey(FitType.FixedRows.ToString()))
        {
            buttonAlignmentSettingDictionary.Add(ButtonAlign.RightLeftTopDown.ToString(),transform.GetComponent<ButtonAlighmentSetting_RightToLeftTopToDown>());
        }
        if(!buttonAlignmentSettingDictionary.ContainsKey(FitType.FixedColumns.ToString()))
        {
            buttonAlignmentSettingDictionary.Add(ButtonAlign.LeftRightDownTop.ToString(),transform.GetComponent<ButtonAlignmentSetting_LeftToRightDownToTop>());
        }
        if(!buttonAlignmentSettingDictionary.ContainsKey(FitType.Custom.ToString()))
        {
            buttonAlignmentSettingDictionary.Add(ButtonAlign.RightLeftDownTop.ToString(),transform.GetComponent<ButtonAlighmentSetting_RightToLeftDownToTop>());
        }
        if(!buttonAlignmentSettingDictionary.ContainsKey(FitType.Custom.ToString()))
        {
            buttonAlignmentSettingDictionary.Add(ButtonAlign.TopDownLeftRight.ToString(),transform.GetComponent<ButtonAlignmentSetting_TopToDownLeftToRight>());
        }
        if(!buttonAlignmentSettingDictionary.ContainsKey(FitType.Custom.ToString()))
        {
            buttonAlignmentSettingDictionary.Add(ButtonAlign.TopDownRightLeft.ToString(),transform.GetComponent<ButtonAlignmentSetting_TopToDownRightToLeft>());
        }
        if(!buttonAlignmentSettingDictionary.ContainsKey(FitType.Custom.ToString()))
        {
            buttonAlignmentSettingDictionary.Add(ButtonAlign.DownTopLeftRight.ToString(),transform.GetComponent<ButtonAlignmentSetting_DownToTopLeftToRight>());
        }
        if(!buttonAlignmentSettingDictionary.ContainsKey(FitType.Custom.ToString()))
        {
            buttonAlignmentSettingDictionary.Add(ButtonAlign.DownTopRightLeft.ToString(),transform.GetComponent<ButtonAlignmentSetting_DownToTopRightToLeft>());
        }
        Debug.Log(rowColumnSettingDictionary.Count);


    }

    public void SetButtonAlignmentSetting()
    {
        Debug.Log("Set");
        if(buttonAlignmentSettingDictionary.ContainsKey(buttonAlignType.ToString()))
        {
            buttonAlignmentSetting=buttonAlignmentSettingDictionary[buttonAlignType.ToString()];
        }
    }
    

    #endregion
    public override void CalculateLayoutInputHorizontal()
    {
        base.CalculateLayoutInputHorizontal();

        AddRowColumnSettingDictionary();
        AddButtonAlignmentSettingDictionary();

        fitTypeBuffer=Fittype;
        Fittype=fitTypeBuffer;

        buttonAlignTypeBuffer=buttonAlignType;
        ButtonAlignType=buttonAlignTypeBuffer;

        #region 不使用
        // if(fitType==FitType.Width||fitType==FitType.Height||fitType==FitType.Uniform)
        // {
        //     fitX=true;
        //     fitY=true;

        //     float sqrt= Mathf.Sqrt(transform.childCount);    //平方根 開根號成float      //transform.child數=>正方形CEIL平方根，假設7，平方根=2.64575 進位成 3 3*3=9>7，創造9格容量容納真實7格
        //     rows= Mathf.CeilToInt(sqrt);      //小數點進位 成INT
        //     columns= Mathf.CeilToInt(sqrt);   //小數點進位 成INT
        // }
        
        
        // /*WIDTH & HEIGHT*/
        // if(fitType==FitType.Width||fitType==FitType.FixedColumns)
        // {
        //     //rows=Mathf.CeilToInt(transform.childCount/(float)columns);
        //     columns=Mathf.CeilToInt(transform.childCount/(float)rows);
        // }
        // if(fitType==FitType.Height||fitType==FitType.FixedRows)
        // {
        //     rows=Mathf.CeilToInt(transform.childCount/(float)columns);
        // }

        // if(fitType==FitType.Custom)
        // {
            
        // }
        #endregion

        rowColumnSetting.RowColumnSet(this.transform,ref fitX,ref fitY,ref rows,ref columns);


        float parentWidth=rectTransform.rect.width;     //取寬
        float parentHeight=rectTransform.rect.height;   //取高

        float cellWidth=parentWidth/(float)columns-/*SPACING REDUCING WIDTH*/((spacing.x/(float)columns)*2)/*PADDING*/-(padding.left/(float)columns)-(padding.right/(float)columns);       //避免超過邊界 ;     //寬/行數=CELL寬
        float cellHeight=parentHeight/(float)rows-/*SPACING REDUCING HEIGHT*/((spacing.y/(float)rows)*2)/*PADDING*/-(padding.top/(float)columns)-(padding.bottom/(float)columns);      //高/列數=CELL高

        cellSize.x= fitX ? cellWidth:cellSize.x;
        cellSize.y= fitY ? cellHeight:cellSize.y;

        int columnCount=0;
        int rowCount=0;

        for(int i=0;i<rectChildren.Count;i++)     //FOR CHILDREN NUMS
        {
            var xPos=0f;
            var yPos=0f;

            buttonAlignmentSetting.SetButtonAlignment(i,rows,columns,ref xPos,ref yPos,cellSize,spacing,ref columnCount,ref rowCount,padding,rows,columns);
            
            #region 取ITEM
            var item=rectChildren[i];
            #endregion

            #region 設定NAME
            item.gameObject.name=rowCount+","+columnCount+": NO."+i;
            Debug.Log("SETNAME");
            #endregion 

            #region 設定TEXT
            item.gameObject.transform.Find("Text").GetComponent<Text>().text=i.ToString();
            #endregion
            
            

            SetChildAlongAxis(item,0,xPos,cellSize.x);        //設定位置
            SetChildAlongAxis(item,1,yPos,cellSize.y);

        }


    }
    public override void CalculateLayoutInputVertical()
    {

    }
    public override void SetLayoutHorizontal()
    {

    }
    public override void SetLayoutVertical()
    {
        
    }
}
