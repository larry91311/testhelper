using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IRowColumnSetting :MonoBehaviour
{   
    // public FlexibleGridLayout flexibleGridLayout;
    // public virtual void OnEnable()
    // {
    //     flexibleGridLayout=GetComponent<FlexibleGridLayout>();
    //     flexibleGridLayout.AddRowColumnSettingDictionary();
    // }
    public abstract void RowColumnSet(Transform theTransform,ref bool fitX,ref bool fitY,ref int rows,ref int columns);
}
