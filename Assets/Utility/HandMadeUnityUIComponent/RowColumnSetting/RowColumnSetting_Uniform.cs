using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RowColumnSetting_Uniform : IRowColumnSetting
{
    public override void RowColumnSet(Transform theTransform,ref bool fitX,ref bool fitY,ref int rows,ref int columns)
    {
        fitX=true;
        fitY=true;

        float sqrt= Mathf.Sqrt(theTransform.childCount);    //平方根 開根號成float      //transform.child數=>正方形CEIL平方根，假設7，平方根=2.64575 進位成 3 3*3=9>7，創造9格容量容納真實7格
        rows= Mathf.CeilToInt(sqrt);      //小數點進位 成INT
        columns= Mathf.CeilToInt(sqrt);   //小數點進位 成INT
    }
}
