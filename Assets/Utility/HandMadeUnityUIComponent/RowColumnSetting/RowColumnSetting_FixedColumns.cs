using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RowColumnSetting_FixedColumns : IRowColumnSetting
{
    public override void RowColumnSet(Transform theTransform,ref bool fitX,ref bool fitY,ref int rows,ref int columns)
    {
        columns=Mathf.CeilToInt(transform.childCount/(float)rows);
    }
}
