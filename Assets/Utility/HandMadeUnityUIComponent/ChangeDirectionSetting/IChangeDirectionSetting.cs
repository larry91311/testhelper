using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IChangeDirectionSetting : MonoBehaviour
{
    public virtual void ChangeDirection(int i,int rows,int columns,ref int rowCount,ref int columnCount,ref float xPos,ref float yPos,Vector2 cellSize)
    {

    }
}
