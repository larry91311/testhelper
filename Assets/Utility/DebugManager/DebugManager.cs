using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugManager
{
    // 帶參數，參數傳入 T1, T2, T3, T4 型別資料
    // public delegate void Print<T>(string t, T obj);
    // public delegate void Print<T1, T2>(string t1, T1 arg1, T2 arg2);
    // public delegate void Print<T1, T2, T3>(string t1, T1 arg1, T2 arg2, T3 arg3);
    // public delegate void Print<T1, T2, T3, T4>(string t1, T1 arg1, T2 arg2, T3 arg3, T4 arg4);
    // public delegate void Print<T1, T2, T3, T4, T5>(string t1, T1 arg1, T2 arg2, T3 arg3, T4 arg4);

    public struct PrintData
    {
        public static void Print<T>(string t, T tt)
        {
            Debug.LogError
            (
                t + ":" + tt
            );
        }

        public static void Print<T1, T2>(string t1, T1 tt1, string t2, T2 tt2)
        {
            Debug.LogError
            (
                t1 + ":" + tt1 + "/" +
                t2 + ":" + tt2
            );
        }

        public static void Print<T1, T2, T3>(string t1, T1 tt1, string t2, T2 tt2, string t3, T3 tt3)
        {
            Debug.LogError
            (
                t1 + ":" + tt1 + "/" +
                t2 + ":" + tt2 + "/" +
                t3 + ":" + tt3
            );
        }

        public static void Print<T1, T2, T3, T4>(string t1, T1 tt1, string t2, T2 tt2, string t3, T3 tt3, string t4, T4 tt4)
        {
            Debug.LogError
            (
                t1 + ":" + tt1 + "/" +
                t2 + ":" + tt2 + "/" +
                t3 + ":" + tt3 + "/" +
                t4 + ":" + tt4
            );
        }

        public static void Print<T1, T2, T3, T4,T5>(string t1, T1 tt1, string t2, T2 tt2, string t3, T3 tt3, string t4, T4 tt4,string t5, T5 tt5)
        {
            Debug.LogError
            (
                t1 + ":" + tt1 + "/" +
                t2 + ":" + tt2 + "/" +
                t3 + ":" + tt3 + "/" +
                t4 + ":" + tt4 + "/" +
                t5 + ":" + tt5
            );
        }
    }
}
